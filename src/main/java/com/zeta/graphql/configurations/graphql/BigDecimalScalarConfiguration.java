package com.zeta.graphql.configurations.graphql;

import graphql.language.StringValue;
import graphql.schema.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

@Configuration
public class BigDecimalScalarConfiguration {

    @Bean
    public GraphQLScalarType bigDecimalScalar() {
        return GraphQLScalarType.newScalar()
                .name("BigDecimal")
                .description("Java BigDecimal as scalar.")
                .coercing(new Coercing<BigDecimal, String >() {
                    @Override
                    public String serialize(final Object dataFetcherResult) {
                        if (dataFetcherResult instanceof BigDecimal) {
                            return dataFetcherResult.toString();
                        } else if (dataFetcherResult instanceof String) {
                            return parseValue(dataFetcherResult).toString();
                        } else {
                            throw new CoercingSerializeException("Expected a BigDecimal object.");
                        }
                    }

                    @Override
                    public BigDecimal parseValue(Object input) {
                        try {
                            if (input instanceof String) {
                                return new BigDecimal(String.valueOf(input));
                            } else {
                                throw new CoercingParseValueException("Expected a String");
                            }
                        } catch (Exception e) {
                            throw new CoercingParseValueException(String.format("Not a valid BigDecimal: '%s'.", input), e
                            );
                        }
                    }

                    @Override
                    public BigDecimal parseLiteral(Object input)  {
                        if (input instanceof StringValue) {
                            try {
                                return new BigDecimal(((StringValue) input).getValue());
                            } catch (Exception e) {
                                throw new CoercingParseLiteralException(e);
                            }
                        } else {
                            throw new CoercingParseLiteralException("Expected a StringValue.");
                        }
                    }
                }).build();
    }
}
