package com.zeta.graphql.dtos;

import com.zeta.graphql.constants.ErrorMessages;
import com.zeta.graphql.exceptions.MandatoryValueException;
import com.zeta.graphql.exceptions.ZetaIllegalActionException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;

/**
 * The information on a book: Models  a book in Zeta
 */
public final class Book {
    private String mainTitle;
    private String subTitle;
    private String isbn;
    private BigDecimal price;
    private List<Long> writers;

    // Not to be instantiated directly by the client
    private Book() {
    }

    // used by the builder
    private Book(BookBuilder builder) {
        mainTitle = builder.mainTitle;
        subTitle = builder.subTitle;
        isbn = builder.isbn;
        price = builder.price;
        writers = builder.writers;
        builder.isInstantiated = true;
    }

    /**
     * Called by the client to obtain a new instance of builder of this class
     * @return {@link BookBuilder}
     */
    public static BookBuilder getBuilder() {
        return BookBuilder.getNewInstance();
    }

    public String getMainTitle() {
        return mainTitle;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public String getIsbn() {
        return isbn;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public List<Long> getWriters() {
        return writers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Book)) return false;

        Book book = (Book) o;

        return new EqualsBuilder()
                .append(getIsbn(), book.getIsbn())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getIsbn())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("mainTitle", mainTitle)
                .append("subTitle", subTitle)
                .append("ISBN", isbn)
                .append("price", price)
                .append("writers", writers)
                .toString();
    }

    /**
     * The builder of the class {@link Book}
     */
    public static final class BookBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(BookBuilder.class);

        private String mainTitle;
        private String subTitle;
        private String isbn;
        private BigDecimal price;
        private List<Long> writers;
        // controls if the builder is already instantiated
        private boolean isInstantiated;

        // Not to be instantiated directly by teh client
        private BookBuilder() {
        }

        // called by the main class in order to obtain a new instance of its own builder
        private static BookBuilder getNewInstance() {
            return new BookBuilder();
        }

        /**
         *
         * @param mainTitle: The main title of the book
         * @return {@link BookBuilder}
         */
        public BookBuilder addMainTitle(final String mainTitle) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.mainTitle)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "mainTitle"));
            }

            if (StringUtils.isBlank(mainTitle)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "mainTitle"));
            }


            LOGGER.info("Adding the main title: {}", mainTitle);
            this.mainTitle = mainTitle;
            return this;
        }

        /**
         *
         * @param subTitle: The subtitle of the book
         * @return {@link BookBuilder}
         */
        public BookBuilder addSubTitle(final String subTitle) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.subTitle)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "subTitle"));
            }

            if (!StringUtils.isBlank(subTitle)) {
                LOGGER.info("Adding the subtitle: {}", subTitle);
                this.subTitle = subTitle;
            }

            return this;
        }

        /**
         *
         * @param ISBN: The International Standard Book Number
         * @return {@link BookBuilder}
         */
        public BookBuilder addIsbn(final String ISBN) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (!StringUtils.isBlank(this.isbn)) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "ISBN"));
            }

            if (StringUtils.isBlank(ISBN)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "ISBN"));
            }

            LOGGER.info("Adding the ISBN: {}", ISBN);
            this.isbn = ISBN;
            return this;
        }

        /**
         *
         * @param price: The price of the book
         * @return {@link BookBuilder}
         */
        public BookBuilder addPrice(final BigDecimal price) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.price) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "price"));
            }

            if (null == price) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "price"));
            }

            LOGGER.info("Adding the price of the book: {}", price);
            this.price = price;
            return this;
        }

        /**
         *
         * @param writers: The writers of the book: The id of the writers: The should exist befor adding the book
         * @return {@link BookBuilder}
         */
        public BookBuilder addWriters(final List<Long> writers) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.writers) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "writers"));
            }

            if (null == writers || writers.isEmpty()) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "writers"));
            }

            LOGGER.info("Adding the writers of the book: {}", writers);
            this.writers = writers;
            return this;
        }


        /**
         * Instantiates the main class and returns it to the client
         * @return {@link Book}
         */
        public Book build() {
            validate();
            return new Book(this);
        }

        // Validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_BUILD_ERR);
            }

            if (StringUtils.isBlank(this.mainTitle)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "mainTitle"));
            }

            if (StringUtils.isBlank(isbn)) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "ISBN"));
            }

            if (null == this.price) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "price"));
            }

            if (null == this.writers || this.writers.isEmpty()) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "writers"));
            }

        }
    }
}
