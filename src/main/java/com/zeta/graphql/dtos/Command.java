package com.zeta.graphql.dtos;

import com.zeta.graphql.constants.ErrorMessages;
import com.zeta.graphql.exceptions.MandatoryValueException;
import com.zeta.graphql.exceptions.ZetaIllegalActionException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * The command of the client: This is the object to be returned to the client for teh query resolver {@link }
 */
public final class Command {

    private Long client;
    private List<ToBeCommandedBook> books;

    // used by Jackson
    // Not to be instantiated directly by the client
    Command() {
    }

    public Command(CommandBuilder builder) {
        client = builder.client;
        books = builder.books;
        builder.isInstantiated = true;
    }

    /**
     * Used by teh client to obtain a new instance of the builder of this class
     * @return {@link CommandBuilder}
     */
    public static CommandBuilder getBuilder() {
        return CommandBuilder.getNewInstance();
    }

    public Long getClient() {
        return client;
    }

    public List<ToBeCommandedBook> getBooks() {
        return books;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Command)) return false;

        Command command = (Command) o;

        return new EqualsBuilder()
                .append(getClient(), command.getClient())
                .append(getBooks(), command.getBooks())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getClient())
                .append(getBooks())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("client", client)
                .append("books", books)
                .toString();
    }

    /**
     * The builder of {@link Command}
     */
    public static final class CommandBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(CommandBuilder.class);

        private Long client;
        private List<ToBeCommandedBook> books;
        // Controls if the builder has been already instantiated
        private boolean isInstantiated;

        // Not to be instantiated directly by the client
        private CommandBuilder() {
        }

        // Used by the main class to obtain a new instance of its own builder
        private static CommandBuilder getNewInstance() {
            return new CommandBuilder();
        }

        /**
         *
         * @param client: The id of the client who is commanding the books
         * @return {@link CommandBuilder}
         */
        public CommandBuilder addClient(final Long client) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.client) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "client"));
            }

            if (null == client) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "client"));
            }

            LOGGER.info("Adding the client of the command with the id {}", client);
            this.client = client;
            return this;
        }

        /**
         *
         * @param books: The list of the id of the books to be added to the command
         * @return {@link CommandBuilder}
         */
        public CommandBuilder addBooks(final List<ToBeCommandedBook> books) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.books) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "books"));
            }

            if (null == books || books.isEmpty()) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "books"));
            }

            LOGGER.info("Adding the the list of the books with the ids {} to the command", books);
            this.books = books;
            return this;
        }

        /**
         * Instantiates the main class and returns this instance to the client
         * @return {@link Command}
         */
        public Command build() {
            validate();
            return new Command(this);
        }

        // validates the integrity of the object before actually building it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.ILLEGAL_BUILD_ERR, "CommandBuilder"));
            }

            if (null == this.client) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "client"));
            }

            if (null == this.books || this.books.isEmpty()) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "books"));
            }
        }

    }
}
