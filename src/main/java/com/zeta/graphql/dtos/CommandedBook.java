package com.zeta.graphql.dtos;

import com.zeta.graphql.constants.ErrorMessages;
import com.zeta.graphql.entities.BookEntity;
import com.zeta.graphql.exceptions.MandatoryValueException;
import com.zeta.graphql.exceptions.ZetaIllegalActionException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The commanded book: The book present in a command
 */
public final class CommandedBook {

    private BookEntity book;
    protected Integer numberOfCopies;

    // used by Jackson
    // Not to be instantiated directly by the client
    CommandedBook() {
    }

    // used by the builder
    private CommandedBook(CommandedBookBuilder builder) {
        book = builder.book;
        numberOfCopies = builder.numberOfCopies;
        builder.isInstantiated = true;
    }

    /**
     * Called by the client to obtain a new instance of the builder of this class
     * @return {@link CommandedBookBuilder}
     */
    public static CommandedBookBuilder getBuilder() {
        return CommandedBookBuilder.getNewInstance();
    }

    public BookEntity getBook() {
        return book;
    }

    public Integer getNumberOfCopies() {
        return numberOfCopies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof CommandedBook)) return false;

        CommandedBook that = (CommandedBook) o;

        return new EqualsBuilder()
                .append(getBook(), that.getBook())
                .append(getNumberOfCopies(), that.getNumberOfCopies())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getBook())
                .append(getNumberOfCopies())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("book", book)
                .append("numberOfCopies", numberOfCopies)
                .toString();
    }

    /**
     * The builder of {@link CommandedBook}
     */
    public static final class CommandedBookBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(CommandedBookBuilder.class);

        private BookEntity book;
        protected Integer numberOfCopies;
        // Controls if the builder is already instantiated
        private boolean isInstantiated;

        // Not to be instantiated directly by the client
        private CommandedBookBuilder() {
        }

        // Called by the main class to obtain a new instance of its own builder
        private static CommandedBookBuilder getNewInstance() {
            return new CommandedBookBuilder();
        }

        /**
         *
         * @param book: The book to be added to the list of the commanded books
         * @return {@link CommandedBookBuilder}
         */
        public CommandedBookBuilder addBook(final BookEntity book) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.book) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "book"));
            }

            if (null == book) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "book"));
            }

            LOGGER.info("Adding book {} to the commanded books", book);
            this.book = book;
            return this;
        }

        /**
         *
         * @param numberOfCopies: Number of the copies of the book in the command
         * @return {@link CommandedBookBuilder}
         */
        public CommandedBookBuilder addNumberOfCopies(final Integer numberOfCopies) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.numberOfCopies) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "numberOfCopies"));
            }

            if (null == numberOfCopies) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "numberOfCopies"));
            }

            LOGGER.info("Adding number of copies of the book {} to the commanded books", numberOfCopies);
            this.numberOfCopies = numberOfCopies;
            return this;
        }

        /**
         * Instantiates teh main class and returns this instance to the client
         * @return {@link CommandedBook}
         */
        public CommandedBook build() {
            validate();
            return new CommandedBook(this);
        }

        // validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.ILLEGAL_BUILD_ERR, "book"));
            }

            if (null == this.book) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "book"));
            }

            if (null == this.numberOfCopies) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "numberOfCopies"));
            }
        }

    }
}
