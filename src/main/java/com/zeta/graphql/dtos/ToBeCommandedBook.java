package com.zeta.graphql.dtos;

import com.zeta.graphql.constants.ErrorMessages;
import com.zeta.graphql.exceptions.MandatoryValueException;
import com.zeta.graphql.exceptions.ZetaIllegalActionException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The book to be included in the command of a client
 */
public final class ToBeCommandedBook {

    private Long book;
    private Integer numberOfCopies;

    // Used By Jackson
    // not to be instantiated directly by the client
    ToBeCommandedBook() {
    }

    // Used by the builder
    private ToBeCommandedBook(ToBeCommandedBookBuilder builder) {
        book = builder.book;
        numberOfCopies = builder.numberOfCopies;
        builder.isInstantiated = true;
    }

    /**
     * To be called by the client to obtain a new instance of the builder of this class
     * @return {@link ToBeCommandedBookBuilder}
     */
    public static ToBeCommandedBookBuilder getBuilder() {
        return ToBeCommandedBookBuilder.getNewInstance();
    }

    public Long getBook() {
        return book;
    }

    public Integer getNumberOfCopies() {
        return numberOfCopies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof ToBeCommandedBook)) return false;

        ToBeCommandedBook that = (ToBeCommandedBook) o;

        return new EqualsBuilder()
                .append(getBook(), that.getBook())
                .append(getNumberOfCopies(), that.getNumberOfCopies())
                .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("book", book)
                .append("numberOfCopies", numberOfCopies)
                .toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getBook())
                .append(getNumberOfCopies())
                .toHashCode();
    }

    /**
     * The builder of {@link ToBeCommandedBook}
     */
    public static final class ToBeCommandedBookBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(ToBeCommandedBookBuilder.class);

        private Long book;
        private Integer numberOfCopies;
        // Controls if the builder is already instantiated
        private boolean isInstantiated;

        // No to be instantiated directly by the client
        private ToBeCommandedBookBuilder() {
        }

        // To be called by the main class to obtain a new instance of its own builder
        private static ToBeCommandedBookBuilder getNewInstance() {
            return new ToBeCommandedBookBuilder();
        }

        /**
         *
         * @param book: The id of the book to be added to the command
         * @return {@link ToBeCommandedBookBuilder}
         */
        public ToBeCommandedBookBuilder addBook(final Long book) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.book) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "book"));
            }

            if (null == book) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "book"));
            }

            LOGGER.info("Adding the book with the id {} to the command", book);
            this.book = book;
            return this;
        }

        /**
         *
         * @param numberOfCopies: The number of copies of the book to be added to the command
         * @return {@link ToBeCommandedBookBuilder}
         */
        public ToBeCommandedBookBuilder addNumberOfCopies(final Integer numberOfCopies) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.numberOfCopies) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "numberOfCopies"));
            }

            if (null == numberOfCopies) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "numberOfCopies"));
            }

            LOGGER.info("Adding the number of copies {} of the book to the command", numberOfCopies);
            this.numberOfCopies = numberOfCopies;
            return this;
        }

        /**
         * Instantiates the main class and returns this instance to the client
         * @return {@link ToBeCommandedBook}
         */
        public ToBeCommandedBook build() {
            validate();
            return new ToBeCommandedBook(this);
        }

        // Verifies the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.ILLEGAL_BUILD_ERR, "ToBeCommandedBookBuilder"));
            }

            if (null == this.book) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "book"));
            }

            if (null == this.numberOfCopies) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "numberOfCopies"));
            }
        }
    }
}
