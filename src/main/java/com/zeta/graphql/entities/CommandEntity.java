package com.zeta.graphql.entities;

import com.zeta.graphql.constants.ErrorMessages;
import com.zeta.graphql.exceptions.MandatoryValueException;
import com.zeta.graphql.exceptions.ZetaIllegalActionException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "commands")
public final class CommandEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "person_id", foreignKey = @ForeignKey(name = "FK_person_command"))
    private PersonEntity client;

    @Column(name = "total_price", columnDefinition = "Decimal(10,2)")
    private BigDecimal totalPrice;

    // Used by Hibernate
    // Not to be directly instantiated by the client
    CommandEntity() {
    }

    // Used by the builder
    private CommandEntity(CommandEntityBuilder builder) {
        client = builder.client;
        totalPrice = builder.totalPrice;
        builder.isInstantiated = true;
    }

    /**
     * Called by the client to obtain a new instance of the builder of this class
     *
     * @return {@link CommandEntityBuilder}
     */
    public static CommandEntityBuilder getBuilder() {
        return CommandEntityBuilder.getNewInstance();
    }

    public Long getId() {
        return id;
    }

    public PersonEntity getClient() {
        return client;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof CommandEntity)) return false;

        CommandEntity that = (CommandEntity) o;

        return new EqualsBuilder()
                .append(getId(), that.getId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getId())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("personEntity", client)
                .append("totalPrice", totalPrice)
                .toString();
    }

    /**
     * The builder of {@link CommandEntity}
     */
    public static final class CommandEntityBuilder {
        private static final Logger LOGGER = LoggerFactory.getLogger(CommandEntityBuilder.class);

        private PersonEntity client;
        private BigDecimal totalPrice;
        // Checks if the builder is already instantiated
        private boolean isInstantiated;

        // Not to be instantiated directly by the client
        private CommandEntityBuilder() {
        }

        // Called by the main class to obtain a new instance of the builder of this class
        private static CommandEntityBuilder getNewInstance() {
            return new CommandEntityBuilder();
        }

        /**
         * @param client: The person whose this command belongs
         * @return {@link CommandEntityBuilder}
         */
        public CommandEntityBuilder addClient(final PersonEntity client) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.client) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "client"));
            }

            if (null == client) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "client"));
            }

            LOGGER.info("Adding Person: {}", client);
            this.client = client;
            return this;
        }

        /**
         * @param totalPrice: The  total price of the command Command of the person
         * @return {@link CommandEntityBuilder}
         */
        public CommandEntityBuilder addTotalPrice(final BigDecimal totalPrice) {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_FEEDING_ERR);
            }

            if (null != this.totalPrice) {
                throw new ZetaIllegalActionException(String.format(ErrorMessages.DUPLICATED_FILED_FEED_ERROR, "totalPrice"));
            }

            if (null == totalPrice) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "totalPrice"));
            }

            LOGGER.info("Adding The total price: {}", totalPrice);
            this.totalPrice = totalPrice;
            return this;
        }

        /**
         * Instantiates the main class and returns this instance to the client
         * @return {@link CommandEntity}
         */
        public CommandEntity build() {
            validate();
            return new CommandEntity(this);
        }

        // Validates the integrity of the object before actually instantiating it
        private void validate() {
            if (this.isInstantiated) {
                throw new ZetaIllegalActionException(ErrorMessages.ILLEGAL_BUILD_ERR);
            }

            if (null == this.client) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "personEntity"));
            }

            if (null == this.totalPrice) {
                throw new MandatoryValueException(String.format(ErrorMessages.MANDATORY_VALUE_ERROR, "totalPrice"));
            }
        }
    }
}
