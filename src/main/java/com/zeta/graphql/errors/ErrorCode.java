package com.zeta.graphql.errors;

public enum ErrorCode {

    RESS_NOT_FOUND("Resource Not Found"),
    ILLEGAL_STATE("Illegal State"),
    RESS_CONFLICT("Resource Conflit"),
    INVALID_VALUE("Invalid Value"),
    INVALID_REQ("Invalid Request"),
    FONCT_ERR("Fuctional Error"),
    INTERN_ERR("Internal Error"),
    REQUIRED_VALUE("Obligatory Parameter"),
    MALFORMED_ERROR("Invalid Argument"),
    INVALID_SIZE("The size of the string invalide"),
    INCOHERENT_VALUE("Incoherent values"),
    ILLEGAL_AGE("The age of the person is illegal for the demanded action"),
    ILLEGAL_ACTION("This action is not permitted");

    private final String description;

    ErrorCode(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}