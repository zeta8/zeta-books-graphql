package com.zeta.graphql.exceptions;


import com.zeta.graphql.errors.ErrorCode;
import graphql.ErrorClassification;
import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.List;

/**
 * <b>The exception thrown when the different input values are not coherent.</b>
 */
public class IncoherentValueException extends ZetaException implements GraphQLError {

    public IncoherentValueException(String message) {
        super(ErrorCode.INCOHERENT_VALUE, message);
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public ErrorClassification getErrorType() {
        return ErrorType.ValidationError;
    }
}
