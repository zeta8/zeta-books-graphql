package com.zeta.graphql.exceptions;


import com.zeta.graphql.errors.ErrorCode;
import graphql.ErrorClassification;
import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.List;

/**
 * <b>The excption thrown in the case of invalid size.</b>
 */
public class InvalidArgumentSizeException extends ZetaException implements GraphQLError {
    public InvalidArgumentSizeException(String message) {
        super(ErrorCode.INVALID_SIZE, message);
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public ErrorClassification getErrorType() {
        return ErrorType.ValidationError;
    }
}
