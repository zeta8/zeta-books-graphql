package com.zeta.graphql.exceptions;


import com.zeta.graphql.errors.ErrorCode;
import graphql.ErrorClassification;
import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.List;


/**
 * <b>The exceptin thrwon when an argument is malformed.</b>
 */
public class MalFormedException extends ZetaException implements GraphQLError {
    public MalFormedException(String message) {
        super(ErrorCode.MALFORMED_ERROR, message);
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public ErrorClassification getErrorType() {
        return ErrorType.ValidationError;
    }
}
