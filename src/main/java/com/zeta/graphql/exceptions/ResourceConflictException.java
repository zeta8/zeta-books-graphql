package com.zeta.graphql.exceptions;


import com.zeta.graphql.errors.ErrorCode;
import graphql.ErrorClassification;
import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.List;

public class ResourceConflictException extends ZetaException implements GraphQLError {

    public ResourceConflictException(String message) {
        super(ErrorCode.RESS_CONFLICT, message);
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public ErrorClassification getErrorType() {
        return ErrorType.ValidationError;
    }
}
