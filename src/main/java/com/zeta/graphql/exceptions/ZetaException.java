package com.zeta.graphql.exceptions;


import com.zeta.graphql.errors.ErrorCode;

/**
 * The main exception of the application.
 */
public class ZetaException extends RuntimeException {

    /**
     * Code de l'exception
     */
    private final ErrorCode code;
    private final static ErrorCode DEFAULT_CODE = ErrorCode.INTERN_ERR;

    /**
     * Constructeur sans paramètre non publique.
     */
    public ZetaException() {
        super();
        this.code = DEFAULT_CODE;
    }

    /**
     * Constructeur portant un message.
     *
     * @param message Un mesage explicite de l'exception.
     */
    public ZetaException(String message) {
        super(message);
        this.code = DEFAULT_CODE;
    }

    /**
     * Constructeur portant un message.
     *
     * @param code    Le code de l'exception
     * @param message Un mesage explicite de l'exception.
     */
    public ZetaException(ErrorCode code, String message) {
        super(message);
        this.code = code;
    }

    /**
     * Récupère le code de l'exception.
     *
     * @return Le code de l'exception.
     */
    public String getCode() {
        return code.name();
    }

    /**
     * Récupère le code de l'exception.
     *
     * @return Le code de l'exception.
     */
    public String getLabel() {
        return code.getDescription();
    }
}
