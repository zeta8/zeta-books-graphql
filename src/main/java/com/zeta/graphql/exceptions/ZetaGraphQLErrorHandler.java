package com.zeta.graphql.exceptions;

import graphql.kickstart.execution.error.GraphQLErrorHandler;
import graphql.ExceptionWhileDataFetching;
import graphql.GraphQLError;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ZetaGraphQLErrorHandler implements GraphQLErrorHandler {

    @Override
    public List<GraphQLError> processErrors(List<GraphQLError> errors) {
        return errors.stream().map(this::getNested).collect(Collectors.toList());
    }

    private GraphQLError getNested(GraphQLError error) {
        if (error instanceof ExceptionWhileDataFetching) {
            ExceptionWhileDataFetching error1 = (ExceptionWhileDataFetching) error;
            if (error1.getException() instanceof GraphQLError) {
                return (GraphQLError) error1.getException();
            }
        }
        return error;
    }

}
