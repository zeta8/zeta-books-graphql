package com.zeta.graphql.exceptions;


import com.zeta.graphql.errors.ErrorCode;
import graphql.ErrorClassification;
import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.List;

/**
 * <b>The exception thrown when the action is not permitted.</b>
 */
public class ZetaIllegalActionException extends ZetaException implements GraphQLError {
    public ZetaIllegalActionException(String message) {
        super(ErrorCode.ILLEGAL_ACTION, message);
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public ErrorClassification getErrorType() {
        return ErrorType.ValidationError;
    }
}
