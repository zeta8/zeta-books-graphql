package com.zeta.graphql.exceptions;

import graphql.ErrorClassification;
import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;
import org.springframework.validation.BindingResult;

import java.util.List;

/**
 * The excceptions thrown by the process of teh validating the input of the service.
 */
public class ZetaMethodArgumentNotValidException extends ZetaException implements GraphQLError {

    private final transient BindingResult bindingResult;

    public ZetaMethodArgumentNotValidException(final BindingResult bindingResult) {
        this.bindingResult = bindingResult;
    }

    public BindingResult getBindingResult() {
        return bindingResult;
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public ErrorClassification getErrorType() {
        return ErrorType.ValidationError;
    }
}
