package com.zeta.graphql.repositories;

import com.zeta.graphql.entities.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BooksRepository extends JpaRepository<BookEntity, Long> {
}
