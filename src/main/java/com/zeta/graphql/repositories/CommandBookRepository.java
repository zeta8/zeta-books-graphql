package com.zeta.graphql.repositories;

import com.zeta.graphql.entities.CommandBookEntity;
import com.zeta.graphql.entities.CommandEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CommandBookRepository extends JpaRepository<CommandBookEntity, Long> {
    Optional<List<CommandBookEntity>> findByCommandEntity(CommandEntity commandEntity);
}
