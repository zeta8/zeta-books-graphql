package com.zeta.graphql.repositories;

import com.zeta.graphql.entities.CommandEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommandsRepository extends JpaRepository<CommandEntity, Long> {
}
