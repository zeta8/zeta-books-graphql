package com.zeta.graphql.repositories;

import com.zeta.graphql.entities.NonEligibilityReasonEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface NonEligibilityReasonRepository extends JpaRepository<NonEligibilityReasonEntity, Long> {
    Optional<NonEligibilityReasonEntity> findByCode(String code);
}
