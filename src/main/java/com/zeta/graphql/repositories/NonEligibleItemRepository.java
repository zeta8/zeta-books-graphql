package com.zeta.graphql.repositories;

import com.zeta.graphql.entities.CommandEntity;
import com.zeta.graphql.entities.NonEligibleItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface NonEligibleItemRepository extends JpaRepository<NonEligibleItemEntity, Long> {
    Optional<List<NonEligibleItemEntity>> findByCommand(CommandEntity command);
}
