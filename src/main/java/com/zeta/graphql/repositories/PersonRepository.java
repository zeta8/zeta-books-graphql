package com.zeta.graphql.repositories;

import com.zeta.graphql.entities.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PersonRepository extends JpaRepository<PersonEntity, Long> {
    Optional<PersonEntity> findByPersonCode(String personCode);
}
