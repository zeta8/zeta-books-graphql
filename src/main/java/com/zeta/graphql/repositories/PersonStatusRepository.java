package com.zeta.graphql.repositories;

import com.zeta.graphql.entities.PersonEntity;
import com.zeta.graphql.entities.PersonStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PersonStatusRepository extends JpaRepository<PersonStatusEntity, Long> {

    Optional<PersonStatusEntity> findByPersonEntity(PersonEntity personEntity);
}
