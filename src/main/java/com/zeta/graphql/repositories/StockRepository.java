package com.zeta.graphql.repositories;

import com.zeta.graphql.entities.BookEntity;
import com.zeta.graphql.entities.StockEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StockRepository extends JpaRepository<StockEntity, Long> {
    Optional<StockEntity> findByBookEntity(BookEntity bookEntity);
}
