package com.zeta.graphql.resolvers.mutations;

import com.zeta.graphql.dtos.Book;
import com.zeta.graphql.entities.BookEntity;
import com.zeta.graphql.services.interfaces.BookService;
import graphql.kickstart.tools.GraphQLMutationResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * The mutatis for books of Zeta
 */
@Component
public class BookMutations implements GraphQLMutationResolver {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookMutations.class);

    private final BookService bookService;

    public BookMutations(final BookService bookService) {
        this.bookService = bookService;
    }

    public BookEntity addBook(final Book book) {
        LOGGER.info("Calling the operation [bookService.addBook] with the parameter {}", book);
        return bookService.addBook(book);
    }

    public List<BookEntity> addBooksFromFile() {
        LOGGER.info("Calling the operation [bookService.addBooksFromFile] with no parameters");
        return bookService.addBooksFromFile();
    }
}
