package com.zeta.graphql.resolvers.mutations;

import com.zeta.graphql.dtos.Command;
import com.zeta.graphql.entities.CommandEntity;
import com.zeta.graphql.services.interfaces.CommandService;
import graphql.kickstart.tools.GraphQLMutationResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.jaas.LoginExceptionResolver;
import org.springframework.stereotype.Component;

/**
 * The mutations for Command
 */
@Component
public class CommandMutations implements GraphQLMutationResolver {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandMutations.class);

    private final CommandService commandService;

    public CommandMutations(final CommandService commandService) {
        this.commandService = commandService;
    }


    public CommandEntity createCommand(final Command command) {
        LOGGER.info("Calling the service §§§§§ with the parameter {}", command);
        return commandService.createCommand(command);
    }
}