package com.zeta.graphql.resolvers.mutations;

import com.zeta.graphql.entities.NonEligibilityReasonEntity;
import com.zeta.graphql.services.interfaces.NonEligibilityReasonsService;
import graphql.kickstart.tools.GraphQLMutationResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Mutations for non-eligibility reason for the items in a command
 */
@Component
public class NonEligibilityReasonsMutation implements GraphQLMutationResolver {
    private static final Logger LOGGER = LoggerFactory.getLogger(NonEligibilityReasonsMutation.class);

    private final NonEligibilityReasonsService nonEligibilityReasonsService;

    public NonEligibilityReasonsMutation(final NonEligibilityReasonsService nonEligibilityReasonsService) {
        this.nonEligibilityReasonsService = nonEligibilityReasonsService;
    }

    public List<NonEligibilityReasonEntity> addNonEligibilityReasonsFromFile() {
        LOGGER.info("Calling teh operation [nonEligibilityReasonsService.addNonEligibilityReasonsFromFile] with no arguments");
        return nonEligibilityReasonsService.addNonEligibilityReasonsFromFile();
    }
}
