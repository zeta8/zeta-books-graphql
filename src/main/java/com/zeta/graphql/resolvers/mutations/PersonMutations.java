package com.zeta.graphql.resolvers.mutations;

import com.zeta.graphql.dtos.PersonInformation;
import com.zeta.graphql.entities.PersonEntity;
import com.zeta.graphql.services.interfaces.PersonService;
import graphql.kickstart.tools.GraphQLMutationResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class PersonMutations implements GraphQLMutationResolver {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonMutations.class);

    private final PersonService personService;

    public PersonMutations(final PersonService personService) {
        this.personService = personService;
    }

    public PersonEntity addPerson(final PersonInformation peron) {
        LOGGER.info("Calling the operation personService.createPerson with teh parameter {}", peron);
        return personService.createPerson(peron);
    }
}