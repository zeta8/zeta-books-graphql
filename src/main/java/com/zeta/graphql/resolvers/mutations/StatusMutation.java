package com.zeta.graphql.resolvers.mutations;

import com.zeta.graphql.dtos.StatusDto;
import com.zeta.graphql.entities.StatusEntity;
import com.zeta.graphql.services.interfaces.StatusService;
import graphql.kickstart.tools.GraphQLMutationResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class StatusMutation implements GraphQLMutationResolver {

    private static final Logger LOGGER = LoggerFactory.getLogger(StatusMutation.class);

    private final StatusService statusService;

    public StatusMutation(final StatusService statusService) {
        this.statusService = statusService;
    }

    public StatusEntity createStatus(final StatusDto status) {
        LOGGER.info("Calling the operation statusService.createStatus with the parameter {}", status);
        return this.statusService.createStatus(status);
    }
}
