package com.zeta.graphql.resolvers.queries;

import com.zeta.graphql.entities.BookEntity;
import com.zeta.graphql.services.interfaces.BookService;
import graphql.kickstart.tools.GraphQLQueryResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class BookQuery implements GraphQLQueryResolver {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookQuery.class);

    private final BookService bookService;

    public BookQuery(final BookService bookService) {
        this.bookService = bookService;
    }

    public BookEntity getBookById(final Long bookId) {
        LOGGER.info("calling the operation bookService.findBookById with the parameter: {}", bookId);
        return bookService.findBookById(bookId);
    }
}
