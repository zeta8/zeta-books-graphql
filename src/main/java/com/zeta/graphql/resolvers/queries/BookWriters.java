package com.zeta.graphql.resolvers.queries;

import com.zeta.graphql.entities.BookEntity;
import com.zeta.graphql.entities.PersonEntity;
import com.zeta.graphql.services.interfaces.BookWriterService;
import graphql.kickstart.tools.GraphQLResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BookWriters implements GraphQLResolver<BookEntity> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookWriters.class);

    private final BookWriterService bookWriterService;

    public BookWriters(final BookWriterService bookWriterService) {
        this.bookWriterService = bookWriterService;
    }

    /**
     * Retrieves the list of writer for the given book
     * @param book: The book
     * @return A list of {@link PersonEntity} who have written the book
     */
    public List<PersonEntity> getWriters(final BookEntity book) {
        LOGGER.info("Calling the operation with the parameter: {}", book);
        return bookWriterService.findWriterByBook(book);
    }
}
