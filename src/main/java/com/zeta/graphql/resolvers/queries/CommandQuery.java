package com.zeta.graphql.resolvers.queries;

import com.zeta.graphql.entities.CommandEntity;
import com.zeta.graphql.services.interfaces.CommandService;
import graphql.kickstart.tools.GraphQLQueryResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * The resolver to get the information on the commands of the client
 */
@Component
public class CommandQuery implements GraphQLQueryResolver {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandQuery.class);

    private final CommandService commandService;

    public CommandQuery(CommandService commandService) {
        this.commandService = commandService;
    }

    public CommandEntity getCommandById(final Long id) {
        LOGGER.info("Calling the operation commandService.findCommandById with teh parameter {}", id);
        return commandService.findCommandById(id);
    }
}
