package com.zeta.graphql.resolvers.queries;

import com.zeta.graphql.dtos.CommandedBook;
import com.zeta.graphql.entities.CommandEntity;
import com.zeta.graphql.services.interfaces.CommandService;
import graphql.kickstart.tools.GraphQLResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * The query to retrieve the books belonging to a command
 */
@Component
public class CommandedBookQuery implements GraphQLResolver<CommandEntity> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandedBookQuery.class);

    private final CommandService commandService;

    public CommandedBookQuery(final CommandService commandService) {
        this.commandService = commandService;
    }


    public List<CommandedBook> getCommandedBooks(final CommandEntity commandEntity) {
        LOGGER.info("Calling teh operation commandService.listBooksInCommand with teh parameter {}", commandEntity);
        return commandService.listBooksInCommand(commandEntity);
    }
}
