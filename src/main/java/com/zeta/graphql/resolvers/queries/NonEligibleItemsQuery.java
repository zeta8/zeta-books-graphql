package com.zeta.graphql.resolvers.queries;

import com.zeta.graphql.entities.CommandEntity;
import com.zeta.graphql.entities.NonEligibleItemEntity;
import com.zeta.graphql.services.interfaces.NonEligibleItemsService;
import graphql.kickstart.tools.GraphQLResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * The query for non eligible items in the command: in the context of commands
 */
@Component
public class NonEligibleItemsQuery implements GraphQLResolver<CommandEntity> {
    private static final Logger LOGGER = LoggerFactory.getLogger(NonEligibleItemsQuery.class);

    private final NonEligibleItemsService nonEligibleItemsService;

    public NonEligibleItemsQuery(final NonEligibleItemsService nonEligibleItemsService) {
        this.nonEligibleItemsService = nonEligibleItemsService;
    }

    public List<NonEligibleItemEntity> getNonEligibleItems(final CommandEntity command) {
        LOGGER.info("Calling the operation [nonEligibleItemsService.findNonEligibleItemsByCommand] with the parameter {}", command);
        return nonEligibleItemsService.findNonEligibleItemsByCommand(command);
    }
}
