package com.zeta.graphql.resolvers.queries;

import com.zeta.graphql.dtos.PersonInformation;
import com.zeta.graphql.entities.PersonEntity;
import com.zeta.graphql.services.interfaces.PersonService;
import graphql.kickstart.tools.GraphQLQueryResolver;
import org.springframework.stereotype.Component;

@Component
public class PersonQuery implements GraphQLQueryResolver {

    private final PersonService personService;

    public PersonQuery(final PersonService personService) {
        this.personService = personService;
    }

    /**
     * Retrieves a person by its id
     * @param personId: The id of person
     * @return {@link PersonInformation}
     */
    PersonEntity findPersonById(final Long personId) {
        return this.personService.findPersonById(personId);
    }

    /**
     * Retrieves a person by its code
     * @param code: The code of person
     * @return {@link PersonEntity}
     */
    PersonEntity findPersonByCode(final String code) {
        return this.personService.findPersonByCode(code);
    }
}
