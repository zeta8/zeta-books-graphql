package com.zeta.graphql.resolvers.queries;

import com.zeta.graphql.entities.PersonEntity;
import com.zeta.graphql.entities.StatusEntity;
import com.zeta.graphql.services.interfaces.PersonStatusService;
import graphql.kickstart.tools.GraphQLResolver;
import org.springframework.stereotype.Component;

@Component
public class PersonStatusQuery implements GraphQLResolver<PersonEntity> {

    private final PersonStatusService personStatusService;

    public PersonStatusQuery(final PersonStatusService personStatusService) {
        this.personStatusService = personStatusService;
    }


    /**
     * Retrieves the status of the person by the information of the person
     * @param personEntity: The person
     * @return {@link StatusEntity}
     */
    StatusEntity getStatus(final PersonEntity personEntity) {
        return this.personStatusService.findForPersonId(personEntity.getId());
    }
}
