package com.zeta.graphql.resolvers.queries;

import com.zeta.graphql.entities.StatusEntity;
import com.zeta.graphql.services.interfaces.StatusService;
import graphql.kickstart.tools.GraphQLQueryResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class StatusQuery implements GraphQLQueryResolver {

    private static final Logger LOGGER = LoggerFactory.getLogger(StatusQuery.class);

    private final StatusService statusService;


    public StatusQuery(final StatusService statusService) {
        this.statusService = statusService;
    }

    /**
     * Retrieves a Status by its ID
     * @param statusId: Teh id of the status
     * @return {@link StatusEntity}
     */
    public StatusEntity findStatusById(final Long statusId) {
        LOGGER.info("Calling the service StatusService.findStatusById with the parameter {}", statusId);
        return this.statusService.findStatusById(statusId);
    }

    /**
     * Retrieves the status by its code
     * @param statusCode: Status code
     * @return {@link StatusEntity}
     */
    public StatusEntity findStatusByCode(final String statusCode) {
        LOGGER.info("Calling the service StatusService.findStatusByCode with the parameter {}", statusCode);
        return this.statusService.findStatusByCode(statusCode);
    }
}
