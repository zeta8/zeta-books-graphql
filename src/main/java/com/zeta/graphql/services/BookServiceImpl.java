package com.zeta.graphql.services;

import com.zeta.graphql.constants.ErrorMessages;
import com.zeta.graphql.dtos.Book;
import com.zeta.graphql.entities.BookEntity;
import com.zeta.graphql.entities.BookWriterEntity;
import com.zeta.graphql.entities.PersonEntity;
import com.zeta.graphql.exceptions.ResourceNotFoundException;
import com.zeta.graphql.filemanagers.CsvFileReader;
import com.zeta.graphql.repositories.BookWriterRepository;
import com.zeta.graphql.repositories.BooksRepository;
import com.zeta.graphql.repositories.PersonRepository;
import com.zeta.graphql.repositories.StockRepository;
import com.zeta.graphql.services.interfaces.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @see BookService
 */
@Service
public class BookServiceImpl implements BookService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookServiceImpl.class);

    @Value("${books.file.name}")
    private String bookFileName;

    private final BooksRepository booksRepository;
    private final BookWriterRepository bookWriterRepository;
    private final PersonRepository personRepository;
    private final StockRepository stockRepository;

    public BookServiceImpl(final BooksRepository booksRepository,
                           final BookWriterRepository bookWriterRepository,
                           final PersonRepository personRepository,
                           final StockRepository stockRepository) {
        this.booksRepository = booksRepository;
        this.bookWriterRepository = bookWriterRepository;
        this.personRepository = personRepository;
        this.stockRepository = stockRepository;
    }

    /**
     * @see BookService#findBookById(Long)
     */
    @Override
    public BookEntity findBookById(final Long id) {
        return booksRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.BOOK_NOT_FOUND, id)));
    }

    /**
     * @see BookService#addBook(Book)
     */
    @Override
    public BookEntity addBook(final Book book) {
        // in order to make the object used in the lambda expression effectively final
        var ref = new Object() {
            BookEntity bookEntity;
        };
        LOGGER.info("Checking if the book isb is already saved in the stock");
        stockRepository.findAll()
                .stream().filter(stock -> stock.getBookEntity().getIsbn().equalsIgnoreCase(book.getIsbn()))
                .findFirst()
                .ifPresentOrElse(
                        stockEntity -> {
                            LOGGER.info("Book Exists in the stock: increasing the number of copies of the existing book");
                            stockRepository.save(stockEntity.addOneToStock());
                            ref.bookEntity = stockEntity.getBookEntity();
                        },
                        () -> {
                            LOGGER.info("Book does not exist in the stock: Adding the book {}", book);
                            ref.bookEntity = booksRepository.save(BookEntity.getBuilder()
                                    .addMainTitle(book.getMainTitle())
                                    .addSubTitle(book.getSubTitle())
                                    .addIsbn(book.getIsbn())
                                    .addPrice(book.getPrice())
                                    .build());

                            addBookWriters(ref.bookEntity, book.getWriters());
                        }
                );

        return ref.bookEntity;
    }

    /**
     * @see BookService#addBookList(List)
     */
    @Override
    public List<BookEntity> addBookList(List<Book> books) {
        LOGGER.info("Adding the list of books {}", books);
        final List<BookEntity> bookEntities = new ArrayList<>();
        books.forEach(book -> bookEntities.add(addBook(book)));
        return bookEntities;
    }

    /**
     * @see BookService#addBooksFromFile()
     */
    @Override
    public List<BookEntity> addBooksFromFile() {
        final List<Book> createdBooks = new ArrayList<>();
        CsvFileReader.getNewInstance()
                .withFileName(bookFileName)
                .readCsvFile()
                .forEach(line -> {
                    final ArrayList<Long> writers = new ArrayList<>();
                    Arrays.stream(Arrays.stream(line[4].split(" "))
                            .toArray()).collect(Collectors.toList())
                            .forEach(element ->
                                    writers.add(
                                            personRepository.findByPersonCode(element.toString().trim())
                                                    .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, element.toString())))
                                                    .getId())
                            );

                    createdBooks.add(
                            Book.getBuilder()
                                    .addMainTitle(line[0])
                                    .addSubTitle(line[1])
                                    .addIsbn(line[2])
                                    .addPrice(new BigDecimal(line[3]))
                                    .addWriters(writers)
                                    .build());
                });
        LOGGER.info("The final result: {}", createdBooks);
        return addBookList(createdBooks);
    }

    // For adding the writers of the book
    private void addBookWriters(final BookEntity book, final List<Long> writers) {
        LOGGER.info("Adding the writers {} for the book {}", writers, book);
        final List<PersonEntity> personEntities = new ArrayList<>();
        writers.forEach(writer -> {
            try {
                final PersonEntity person = personRepository.findById(writer)
                        .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, writer)));
                personEntities.add(person);
            } catch (ResourceNotFoundException ex) {
                LOGGER.error("Rollback: The writer with the id {} does not exist. Deleting the book {}", writer, book);
                booksRepository.delete(book);
                throw ex;
            }
        });

        personEntities.forEach(person ->
                bookWriterRepository.save(
                        BookWriterEntity.getBuilder()
                                .addBook(book)
                                .addWriter(person)
                                .build()
                ));
    }
}
