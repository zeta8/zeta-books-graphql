package com.zeta.graphql.services;

import com.zeta.graphql.constants.ErrorMessages;
import com.zeta.graphql.entities.BookEntity;
import com.zeta.graphql.entities.BookWriterEntity;
import com.zeta.graphql.entities.PersonEntity;
import com.zeta.graphql.exceptions.ResourceNotFoundException;
import com.zeta.graphql.repositories.BookWriterRepository;
import com.zeta.graphql.repositories.BooksRepository;
import com.zeta.graphql.repositories.PersonRepository;
import com.zeta.graphql.services.interfaces.BookWriterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @see BookWriterService
 */
@Service
public class BookWriterServiceImpl implements BookWriterService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookWriterServiceImpl.class);

    private final BookWriterRepository bookWriterRepository;

    public BookWriterServiceImpl(final BookWriterRepository bookWriterRepository) {
        this.bookWriterRepository = bookWriterRepository;
    }

    @Override
    public List<PersonEntity> findWriterByBook(BookEntity book) {
        LOGGER.info("Searching for the writers of the book with id: {}", book.getId());
        return bookWriterRepository.findByBookEntity(book)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, book.getId())))
                .stream().map(BookWriterEntity::getPersonEntity)
                .collect(Collectors.toList());
    }
}
