package com.zeta.graphql.services;

import com.zeta.graphql.constants.ErrorMessages;
import com.zeta.graphql.dtos.Command;
import com.zeta.graphql.dtos.CommandedBook;
import com.zeta.graphql.entities.*;
import com.zeta.graphql.enums.NonEligibilityReasonEnum;
import com.zeta.graphql.exceptions.ResourceNotFoundException;
import com.zeta.graphql.exceptions.ZetaIllegalStateException;
import com.zeta.graphql.repositories.*;
import com.zeta.graphql.services.interfaces.CommandService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * @see CommandService
 */
@Service
public class CommandServiceImpl implements CommandService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandServiceImpl.class);

    private final CommandsRepository commandsRepository;
    private final CommandBookRepository commandBookRepository;
    private final StockRepository stockRepository;
    private final BooksRepository booksRepository;
    private final PersonRepository personRepository;
    private final NonEligibleItemRepository nonEligibleItemRepository;
    private final NonEligibilityReasonRepository nonEligibilityReasonRepository;

    public CommandServiceImpl(final CommandsRepository commandsRepository,
                              final CommandBookRepository commandBookRepository,
                              final StockRepository stockRepository,
                              final BooksRepository booksRepository,
                              final PersonRepository personRepository,
                              final NonEligibleItemRepository nonEligibleItemRepository,
                              final NonEligibilityReasonRepository nonEligibilityReasonRepository) {
        this.commandsRepository = commandsRepository;
        this.commandBookRepository = commandBookRepository;
        this.stockRepository = stockRepository;
        this.booksRepository = booksRepository;
        this.personRepository = personRepository;
        this.nonEligibleItemRepository = nonEligibleItemRepository;
        this.nonEligibilityReasonRepository = nonEligibilityReasonRepository;
    }

    /**
     * @see CommandService#findCommandById(Long)
     */
    @Override
    public CommandEntity findCommandById(final Long commandId) {
        LOGGER.info("Retrieving the command with the id {}", commandId);
        return commandsRepository.findById(commandId)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, commandId)));
    }

    /**
     * @see CommandService#findCommandsByPersonId(Long)
     */
    @Override
    public List<CommandEntity> findCommandsByPersonId(Long personId) {
        return null;
    }

    /**
     * @see CommandService#findCommandsByPersonCode(String)
     */
    @Override
    public List<CommandEntity> findCommandsByPersonCode(String personCode) {
        return null;
    }

    /**
     * @see CommandService#createCommand(Command)
     */
    @Override
    public CommandEntity createCommand(final Command command) {
        LOGGER.info("Gathering the information on the client with the id {}", command.getClient());
        final PersonEntity client = personRepository.findById(command.getClient())
                .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, command.getClient())));

        LOGGER.info("Checking if there are eligible elements in the command list");
        final Map<BookEntity, Integer> eligibleBooksMap = new HashMap<>();
        final AtomicReference<BigDecimal> totalPrice = new AtomicReference<>(new BigDecimal("0.00"));
        final List<BookEntity> notEnoughCopies = new ArrayList<>();
        final List<Long> notFoundBooks = new ArrayList<>();
        // here we filter the books that could be added to the commands.
        // Only the existing books whose number of demanded copies are less than or equal to
        // the number of copies available in the stock are eligible to be added to the command.
        // here the total price for the eligible books has also been calculated
        // and the non-eligible items are divided into to categories:
        // 1- books that are not found,
        // 2- books that are not of enough number of copies to satisfy the demand of the client
        // if there are such book, they wil be added to the response of the service and
        // saved for statistics and commercial use
        command.getBooks().forEach(entry ->
                booksRepository.findById(entry.getBook())
                        .ifPresentOrElse(bookEntity -> stockRepository.findByBookEntity(bookEntity)
                                .ifPresentOrElse(stockEntity -> {
                                            if (stockEntity.getNumberOfCopies() >= entry.getNumberOfCopies()) {
                                                eligibleBooksMap.put(bookEntity, entry.getNumberOfCopies());
                                                stockRepository.save(stockEntity.subtractFromStock(entry.getNumberOfCopies()));
                                                totalPrice.set(
                                                        totalPrice.get()
                                                                .add(bookEntity.getPrice()
                                                                        .multiply(
                                                                                new BigDecimal(entry.getNumberOfCopies()))));
                                            } else {
                                                notEnoughCopies.add(bookEntity);
                                            }
                                        }, () -> notFoundBooks.add(entry.getBook())
                                ), () -> notFoundBooks.add(entry.getBook())
                        ));
        // if the command contains no valid item it will not be created; an exception will raise instead
        LOGGER.info("Checking if the command contains any valid item");
        if (eligibleBooksMap.isEmpty()) {
            throw new ZetaIllegalStateException(String.format(ErrorMessages.COMMAND_NOT_VALID, "The books in your command are not accessible for the moment"));
        }

        LOGGER.info("Saving the command for the client {}", client);
        final CommandEntity commandEntity = commandsRepository.save(CommandEntity.getBuilder()
                .addClient(client)
                .addTotalPrice(totalPrice.get())
                .build());

        // saves the eligible items among the commands
        LOGGER.info("Adding books to the commanded books");
        eligibleBooksMap.forEach((key, value) ->
                commandBookRepository.save(CommandBookEntity.getBuilder()
                        .addBookEntity(key)
                        .addNumberOfCopies(value)
                        .addCommandEntity(commandEntity)
                        .build()));

        // adds the non-found books to the non-eligible elements
        LOGGER.info("Adding the non found books to the non-eligible items");
        notFoundBooks.forEach(entry ->
        {
            final NonEligibilityReasonEntity nonEligibilityReasonEntity =
                    nonEligibilityReasonRepository.findByCode(NonEligibilityReasonEnum.NOT_FOUND.name())
                            .orElseThrow(() ->
                                    new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, NonEligibilityReasonEnum.NOT_FOUND.name())));

            nonEligibleItemRepository.save(NonEligibleItemEntity.getBuilder()
                    .addNonExistingBook(entry)
                    .addReason(nonEligibilityReasonEntity)
                    .addCommand(commandEntity)
                    .build());
        });

        // adds the books which are not found with enough number of copies
        // in the stock to the non-eligible elements
        LOGGER.info("Adding the books with not enough number of copies to the non-eligible items");
        notEnoughCopies.forEach(entry -> {
            final NonEligibilityReasonEntity nonEligibilityReasonEntity =
                    nonEligibilityReasonRepository.findByCode(NonEligibilityReasonEnum.NOT_ENOUGH_COPIES.name())
                            .orElseThrow(() ->
                                    new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR,
                                            NonEligibilityReasonEnum.NOT_ENOUGH_COPIES.name())));

            nonEligibleItemRepository.save(NonEligibleItemEntity.getBuilder()
                    .addBook(entry)
                    .addReason(nonEligibilityReasonEntity)
                    .addCommand(commandEntity)
                    .build());
        });

        return commandEntity;
    }

    @Override
    public List<CommandedBook> listBooksInCommand(final CommandEntity commandEntity) {
        return commandBookRepository.findByCommandEntity(commandEntity)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, commandEntity.getId())))
                .stream().map(this::constructCommandedBook)
                .collect(Collectors.toList());
    }


    // Construct an instance of the CommandedBook
    private CommandedBook constructCommandedBook(final CommandBookEntity commandBookEntity) {
        return CommandedBook.getBuilder()
                .addBook(commandBookEntity.getBookEntity())
                .addNumberOfCopies(commandBookEntity.getNumberOfCopies())
                .build();
    }
}
