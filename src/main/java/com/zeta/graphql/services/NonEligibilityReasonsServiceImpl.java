package com.zeta.graphql.services;


import com.zeta.graphql.entities.NonEligibilityReasonEntity;
import com.zeta.graphql.filemanagers.CsvFileReader;
import com.zeta.graphql.repositories.NonEligibilityReasonRepository;
import com.zeta.graphql.services.interfaces.NonEligibilityReasonsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @see NonEligibilityReasonsService
 */
@Service
public class NonEligibilityReasonsServiceImpl implements NonEligibilityReasonsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NonEligibilityReasonsServiceImpl.class);

    @Value("${reasons.file.name}")
    private String reasonFileName;

    private final NonEligibilityReasonRepository nonEligibilityReasonRepository;


    public NonEligibilityReasonsServiceImpl(final NonEligibilityReasonRepository nonEligibilityReasonRepository) {
        this.nonEligibilityReasonRepository = nonEligibilityReasonRepository;
    }

    /**
     * @see NonEligibilityReasonsService#addNonEligibilityReasonsFromFile()
     */
    @Override
    public List<NonEligibilityReasonEntity> addNonEligibilityReasonsFromFile() {
        final AtomicReference<List<NonEligibilityReasonEntity>> reasonsAtomicReference = new AtomicReference<>(new ArrayList<>());
        CsvFileReader.getNewInstance()
                .withFileName(reasonFileName)
                .readCsvFile()
                .forEach(line ->
                    nonEligibilityReasonRepository.findByCode(line[0])
                            .ifPresentOrElse(reason -> reasonsAtomicReference.get().add(reason),
                                    () -> reasonsAtomicReference.get()
                                            .add(nonEligibilityReasonRepository.save(
                                                    NonEligibilityReasonEntity.getBuilder()
                                                            .addCode(line[0])
                                                            .addLabel(line[1])
                                                            .addDescription(line[2])
                                                            .build())
                                            ))
                );
        return reasonsAtomicReference.get();
    }
}
