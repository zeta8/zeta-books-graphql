package com.zeta.graphql.services;

import com.zeta.graphql.entities.CommandEntity;
import com.zeta.graphql.entities.NonEligibleItemEntity;
import com.zeta.graphql.repositories.NonEligibleItemRepository;
import com.zeta.graphql.services.interfaces.NonEligibleItemsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @see NonEligibleItemsService
 */
@Service
public class NonEligibleItemsServiceImpl implements NonEligibleItemsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NonEligibleItemsServiceImpl.class);

    private final NonEligibleItemRepository nonEligibleItemRepository;

    public NonEligibleItemsServiceImpl(final NonEligibleItemRepository nonEligibleItemRepository) {
        this.nonEligibleItemRepository = nonEligibleItemRepository;
    }

    /**
     * @see NonEligibleItemsService#findNonEligibleItemsByCommand(CommandEntity)
     */
    @Override
    public List<NonEligibleItemEntity> findNonEligibleItemsByCommand(final CommandEntity command) {
        LOGGER.info("Searching for non-eligible items for the command with the id {} for the client with the id {}",
                command.getId(), command.getClient().getId());
        final AtomicReference<List<NonEligibleItemEntity>> nonEligibleItems = new AtomicReference<>(new ArrayList<>());
        nonEligibleItemRepository.findByCommand(command)
                .ifPresentOrElse(nonEligibleItems::set,
                        () -> nonEligibleItems.set(new ArrayList<>()));
        return nonEligibleItems.get();
    }

}
