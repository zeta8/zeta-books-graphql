package com.zeta.graphql.services;

import com.zeta.graphql.constants.ErrorMessages;
import com.zeta.graphql.dtos.PersonInformation;
import com.zeta.graphql.entities.PersonEntity;
import com.zeta.graphql.entities.PersonStatusEntity;
import com.zeta.graphql.entities.StatusEntity;
import com.zeta.graphql.exceptions.ResourceNotFoundException;
import com.zeta.graphql.repositories.PersonRepository;
import com.zeta.graphql.repositories.PersonStatusRepository;
import com.zeta.graphql.repositories.StatusRepository;
import com.zeta.graphql.services.interfaces.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @see PersonService
 */
@Service
public class PersonServiceImpl implements PersonService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonServiceImpl.class);

    private final StatusRepository statusRepository;
    private final PersonRepository personRepository;
    private final PersonStatusRepository personStatusRepository;

    public PersonServiceImpl(final StatusRepository statusRepository,
                             final PersonRepository personRepository,
                             final PersonStatusRepository personStatusRepository) {
        this.statusRepository = statusRepository;
        this.personRepository = personRepository;
        this.personStatusRepository = personStatusRepository;
    }

    /**
     * @see PersonService#createPerson(PersonInformation)
     */
    @Override
    public PersonEntity createPerson(final PersonInformation person) {
        // at first we verify that the status exists
        StatusEntity statusEntity;
        try {
            statusEntity = statusRepository.findByCode(person.getStatus().name())
                    .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, person.getStatus().name())));
        } catch (ResourceNotFoundException exception) {
            LOGGER.error("The Status code {} not found for the person with the code {}", person.getStatus().name(), person.getPersonCode());
            throw exception;
        }

        final PersonEntity personEntity = personRepository.save(PersonEntity.getBuilder()
                .addPersonCode(person.getPersonCode())
                .addFirstName(person.getFirstName())
                .addMiddleName(person.getMiddleName())
                .addLastName(person.getLastName())
                .addBirthDate(person.getBirthDate())
                .build());

        personStatusRepository.save(
                PersonStatusEntity.getBuilder()
                        .addPersonEntity(personEntity)
                        .addStatusEntity(statusEntity)
                        .build());

        return personEntity;
    }

    @Override
    public PersonEntity findPersonById(Long personId) {
        return personRepository.findById(personId)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, personId)));
    }

    @Override
    public PersonEntity findPersonByCode(final String code) {
        return personRepository.findByPersonCode(code)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, code)));
    }
}
