package com.zeta.graphql.services;

import com.zeta.graphql.constants.ErrorMessages;
import com.zeta.graphql.entities.PersonEntity;
import com.zeta.graphql.entities.PersonStatusEntity;
import com.zeta.graphql.entities.StatusEntity;
import com.zeta.graphql.exceptions.ResourceNotFoundException;
import com.zeta.graphql.repositories.PersonRepository;
import com.zeta.graphql.repositories.PersonStatusRepository;
import com.zeta.graphql.services.interfaces.PersonStatusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @see PersonStatusService
 */
@Service
public class PersonStatusServiceImpl implements PersonStatusService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonStatusServiceImpl.class);

    private final PersonRepository personRepository;
    private final PersonStatusRepository personStatusRepository;

    public PersonStatusServiceImpl(final PersonRepository personRepository,
                                   final PersonStatusRepository personStatusRepository) {
        this.personRepository = personRepository;
        this.personStatusRepository = personStatusRepository;
    }

    /**
     * @see PersonStatusService#findByPersonId(Long)
     */
    @Override
    public PersonStatusEntity findByPersonId(final Long personId) {
        LOGGER.info("Retrieving person status entity for person with the id: {}", personId);
        return personStatusRepository.findByPersonEntity(
                personRepository.findById(personId)
                        .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, personId)))
        ).orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, personId)));
    }

    /**
     * @see PersonStatusService#findByPersonEntity(PersonEntity)
     */
    @Override
    public PersonStatusEntity findByPersonEntity(final PersonEntity personEntity) {
        LOGGER.info("Retrieving person status entity for person with the id: {}", personEntity.getId());
        return personStatusRepository.findByPersonEntity(personEntity)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, personEntity.getId())));
    }

    /**
     * @see PersonStatusService#createPersonStatus(PersonEntity, StatusEntity)
     */
    @Override
    public PersonStatusEntity createPersonStatus(final PersonEntity personEntity, final StatusEntity statusEntity) {
        LOGGER.info("creating person status with the parameters {} and {}", personEntity, statusEntity);
        return personStatusRepository
                .save(PersonStatusEntity.getBuilder()
                        .addPersonEntity(personEntity)
                        .addStatusEntity(statusEntity)
                        .build());
    }

    @Override
    public StatusEntity findForPersonId(final Long personId) {
        LOGGER.info("Retrieving the status  for person with the id: {}", personId);
        return personStatusRepository.findByPersonEntity(
                personRepository.findById(personId)
                        .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, personId)))
        ).orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, personId)))
                .getStatusEntity();
    }
}
