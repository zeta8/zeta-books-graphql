package com.zeta.graphql.services;

import com.zeta.graphql.constants.ErrorMessages;
import com.zeta.graphql.dtos.StatusDto;
import com.zeta.graphql.entities.StatusEntity;
import com.zeta.graphql.exceptions.ResourceNotFoundException;
import com.zeta.graphql.repositories.StatusRepository;
import com.zeta.graphql.services.interfaces.StatusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @see StatusService
 */
@Service
public class StatusServiceImpl implements StatusService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StatusServiceImpl.class);

    private final StatusRepository statusRepository;

    public StatusServiceImpl(final StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    /**
     * @see StatusService#createStatus(StatusDto)
     */
    @Override
    public StatusEntity createStatus(StatusDto status) {
        LOGGER.info("Crating the status: {}", status);
        return statusRepository.save(
                StatusEntity.getBuilder()
                .addCode(status.getCode())
                .addLabel(status.getLabel())
                .addDescription(status.getDescription())
                .build());
    }

    /**
     * @see StatusService#findStatusById(Long)
     */
    @Override
    public StatusEntity findStatusById(final Long statusId) {
        return statusRepository.findById(statusId)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, statusId)));
    }

    /**
     * @see StatusService#findStatusByCode(String)
     */
    @Override
    public StatusEntity findStatusByCode(String statusCode) {
        return statusRepository.findByCode(statusCode)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(ErrorMessages.NOT_FOUND_ERR, statusCode)));
    }
}
