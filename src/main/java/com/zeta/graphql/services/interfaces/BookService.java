package com.zeta.graphql.services.interfaces;

import com.zeta.graphql.dtos.Book;
import com.zeta.graphql.entities.BookEntity;

import java.util.List;


/**
 * A collection of CRUD operations on Book
 */
public interface BookService {

    /**
     * Retrieves a book by its id
     * @param id: The id of the book
     * @return {@link BookEntity}
     */
    BookEntity findBookById(Long id);

    /**
     * Adds a new book and returns it along with teh id of the recently created book to the client
     * @param book: The book to be added
     * @return {@link BookEntity}
     */
    BookEntity addBook(Book book);

    /**
     * Adds a list of the books and returns this list along the id of each recently created book to the client
     * @param books: A list of {@link Book}
     * @return the list of recently created {@link Book}
     */
    List<BookEntity> addBookList(List<Book> books);


    /**
     * Adds a list of books from a predefined file
     * @return a list of {@link BookEntity}
     */
    List<BookEntity> addBooksFromFile();
}
