package com.zeta.graphql.services.interfaces;

import com.zeta.graphql.entities.BookEntity;
import com.zeta.graphql.entities.PersonEntity;

import java.util.List;

/**
 * Collection of CRUD operations on BookWriter.
 */
public interface BookWriterService {

    /**
     * Retrieves the list of writers fr a given book
     * @param book : The book which the <writers are being searched
     * @return {@link PersonEntity}
     */
    List<PersonEntity> findWriterByBook(BookEntity book);
}
