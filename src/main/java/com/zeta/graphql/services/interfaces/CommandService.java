package com.zeta.graphql.services.interfaces;

import com.zeta.graphql.dtos.Command;
import com.zeta.graphql.dtos.CommandedBook;
import com.zeta.graphql.entities.CommandEntity;

import java.util.List;

/**
 * Collection of operations for CRUD operations on the commands of teh client
 */
public interface CommandService {

    /**
     * Retrieves a command by its id
     * @param commandId: The id of the command
     * @return {@link CommandEntity}
     */
    CommandEntity findCommandById(Long commandId);

    /**
     * Retrieves the list of all commands of a person by its id
     * @param personId: the id of the person
     * @return A list of {@link CommandEntity}
     */
    List<CommandEntity> findCommandsByPersonId(Long personId);

    /**
     * Retrieves the list of all commands of a person by its code
     * @param personCode: the code of the person
     * @return A list of {@link CommandEntity}
     */
    List<CommandEntity> findCommandsByPersonCode(String personCode);

    /**
     * Creates a new command for a client and returns it back to client along with the id of rhe created command
     * @param command: The information on teh command to be created
     * @return {@link CommandEntity}
     */
    CommandEntity createCommand(Command command);

    /**
     * Lists all books included in a specific command
     * @param commandEntity: The command for which the list of books are searched
     * @return A list of {@link CommandedBook}
     */
    List<CommandedBook> listBooksInCommand(CommandEntity commandEntity);
}
