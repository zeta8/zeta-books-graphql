package com.zeta.graphql.services.interfaces;

import com.zeta.graphql.entities.NonEligibilityReasonEntity;

import java.util.List;

/**
 * colection of operation to do CRUD operations on the reason of non-eligibility of items in a command
 */
public interface NonEligibilityReasonsService {

    /**
     * Adds the non-eligibility reasons code-label-description from the existing file
     * @return a list of {@link NonEligibilityReasonEntity}
     */
    List<NonEligibilityReasonEntity> addNonEligibilityReasonsFromFile();
}
