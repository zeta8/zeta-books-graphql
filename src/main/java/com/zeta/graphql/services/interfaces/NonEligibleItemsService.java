package com.zeta.graphql.services.interfaces;

import com.zeta.graphql.entities.CommandEntity;
import com.zeta.graphql.entities.NonEligibleItemEntity;

import java.util.List;

/**
 * A collection of operations for controlling the non-eligible items in the commands: mainly for commercial use
 */
public interface NonEligibleItemsService {

    /**
     * Retrieves the list of non-eligible items in a command
     * @param command: The command in which the non-eligible items are searched
     * @return a list of {@link NonEligibleItemEntity}
     */
    List<NonEligibleItemEntity> findNonEligibleItemsByCommand(CommandEntity command);
}
