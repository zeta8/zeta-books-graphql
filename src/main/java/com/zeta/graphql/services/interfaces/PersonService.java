package com.zeta.graphql.services.interfaces;

import com.zeta.graphql.dtos.PersonInformation;
import com.zeta.graphql.entities.PersonEntity;

/**
 * A collection of operations for doing crud operations on Person
 */
public interface PersonService {

    /**
     *
     * @param person: person to be created
     * @return {@link PersonEntity}
     */
    PersonEntity createPerson(PersonInformation person);

    /**
     * Retrieves a person by its ID
     * @param personId: The id of person
     * @return {@link PersonInformation}
     */
    PersonEntity findPersonById(Long personId);

    /**
     * Retrieves a person by its code
     * @param code: the code of person
     * @return {@link PersonEntity}
     */
    PersonEntity findPersonByCode(String code);
}
