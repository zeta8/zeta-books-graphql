package com.zeta.graphql.services.interfaces;

import com.zeta.graphql.entities.PersonEntity;
import com.zeta.graphql.entities.PersonStatusEntity;
import com.zeta.graphql.entities.StatusEntity;

/**
 * Collection of operations to do crud operations on person status
 */
public interface PersonStatusService {

    /**
     * Finds the person status by the id of the person
     * @param personId: The id of the person
     * @return {@link PersonStatusEntity}
     */
    PersonStatusEntity findByPersonId(Long personId);


    /**
     * Retrieved a PersonStatus for an existing person
     * @param personEntity: The person for whom the status is going to be retrieved
     * @return {@link PersonStatusEntity}
     */
    PersonStatusEntity findByPersonEntity(PersonEntity personEntity);

    /**
     * Creates a new Person Status entity
     * @param personEntity: The person fow whom the person sttaus is being created
     * @param statusEntity: The status to be linked to the person
     * @return {@link PersonStatusEntity}
     */
    PersonStatusEntity createPersonStatus(PersonEntity personEntity, StatusEntity statusEntity);

    /**
     * Retrieves Person Status by its ID
     * @param personId: Id of person
     * @return {@link StatusEntity}
     */
    StatusEntity findForPersonId(Long personId);
}
