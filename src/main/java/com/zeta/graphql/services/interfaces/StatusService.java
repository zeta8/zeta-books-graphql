package com.zeta.graphql.services.interfaces;

import com.zeta.graphql.dtos.StatusDto;
import com.zeta.graphql.entities.StatusEntity;

/**
 * Operations for crud operations on Status
 */
public interface StatusService {

    /**
     * Creates a new Status for person
     * @param statusDto: The information on the status to be created
     * @return: {@link StatusEntity}
     */
    StatusEntity createStatus(StatusDto statusDto);

    /**
     * Retrieves a status by its Id
     * @param statusId: The id of teh status
     * @return {@link StatusEntity}
     */
    StatusEntity findStatusById(Long statusId);

    /**
     * Retrieves a status by its code
     * @param statusCode: The code of the status
     * @return {@link StatusEntity}
     */
    StatusEntity findStatusByCode(String statusCode);
}
