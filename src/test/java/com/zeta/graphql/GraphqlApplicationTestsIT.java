package com.zeta.graphql;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@SpringBootTest(classes = GraphqlApplication.class,
		webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GraphqlApplicationTestsIT {

	@Test
	void contextLoads() {
	}

}
