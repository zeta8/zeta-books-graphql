package com.zeta.graphql.dtos;

import com.zeta.graphql.exceptions.MandatoryValueException;
import com.zeta.graphql.exceptions.ZetaIllegalActionException;
import com.zeta.graphql.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.NullSource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BookTest {

    private static final String MAIN_TITLE = "The Adapted mind";
    private static final String SUB_TITLE = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN = "0-19-506023-7";
    private static final BigDecimal PRICE = new BigDecimal("25.49");
    private static final List<Long> writers = new ArrayList<>(){
        {
            add(1L);
            add(2L);
            add(3L);
        }
    };

    @Test
    void when_all_fields_fed_correctly_then_should_be_ok() {
        // given: the input

        // when
        final Book book = Book.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addIsbn(ISBN)
                .addPrice(PRICE)
                .addWriters(writers)
                .build();

        // then
        assertNotNull(book);
        assertEquals(MAIN_TITLE, book.getMainTitle());
        assertEquals(SUB_TITLE, book.getSubTitle());
        assertEquals(ISBN, book.getIsbn());
        assertEquals(PRICE, book.getPrice());
        assertEquals(writers, book.getWriters());
    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_subTitle_fed_null_and_other_fields_fed_correctly_then_should_be_ok(final String subTitle) {
        // given: the input

        // when
        final Book book = Book.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(subTitle)
                .addIsbn(ISBN)
                .addPrice(PRICE)
                .addWriters(writers)
                .build();

        // then
        assertNotNull(book);
        assertEquals(MAIN_TITLE, book.getMainTitle());
        assertNull(book.getSubTitle());
        assertEquals(ISBN, book.getIsbn());
        assertEquals(PRICE, book.getPrice());
        assertEquals(writers, book.getWriters());
    }

    @Test
    void when_subTitle_not_fed_and_other_fields_fed_correctly_then_should_be_ok() {
        // given: the input

        // when
        final Book book = Book.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addIsbn(ISBN)
                .addPrice(PRICE)
                .addWriters(writers)
                .build();

        // then
        assertNotNull(book);
        assertEquals(MAIN_TITLE, book.getMainTitle());
        assertNull(book.getSubTitle());
        assertEquals(ISBN, book.getIsbn());
        assertEquals(PRICE, book.getPrice());
        assertEquals(writers, book.getWriters());
    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_mainTitle_fed_null_or_empty_then_should_throw_MandatoryValueException(final String mainTitle) {
        // given: the input
        final Book.BookBuilder bookBuilder = Book.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> bookBuilder.addMainTitle(mainTitle));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_mainTitle_not_fed_then_should_throw_MandatoryValueException() {
        // given: the input
        final Book.BookBuilder bookBuilder = Book.getBuilder()
                .addIsbn(ISBN)
                .addPrice(PRICE)
                .addWriters(writers);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, bookBuilder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_isbn_fed_null_or_empty_then_should_throw_MandatoryValueException(final String isbn) {
        // given: the input
        final Book.BookBuilder bookBuilder = Book.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> bookBuilder.addIsbn(isbn));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_isbn_not_fed_then_should_throw_MandatoryValueException() {
        // given: the input
        final Book.BookBuilder bookBuilder = Book.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addPrice(PRICE)
                .addWriters(writers);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, bookBuilder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_price_fed_null_or_empty_then_should_throw_MandatoryValueException(final BigDecimal price) {
        // given: the input
        final Book.BookBuilder bookBuilder = Book.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> bookBuilder.addPrice(price));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_price_not_fed_then_should_throw_MandatoryValueException() {
        // given: the input
        final Book.BookBuilder bookBuilder = Book.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addIsbn(ISBN)
                .addWriters(writers);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, bookBuilder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_writers_fed_null_or_empty_then_should_throw_MandatoryValueException(final List<Long> writers) {
        // given: the input
        final Book.BookBuilder bookBuilder = Book.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> bookBuilder.addWriters(writers));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_writers_not_fed_then_should_throw_MandatoryValueException() {
        // given: the input
        final Book.BookBuilder bookBuilder = Book.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addIsbn(ISBN)
                .addPrice(PRICE);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, bookBuilder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_mainTitle_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given: the input
        final Book.BookBuilder bookBuilder = Book.getBuilder()
                .addMainTitle(MAIN_TITLE);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> bookBuilder.addMainTitle(MAIN_TITLE));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_subTitle_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given: the input
        final Book.BookBuilder bookBuilder = Book.getBuilder()
                .addSubTitle(SUB_TITLE);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> bookBuilder.addSubTitle(SUB_TITLE));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_isbn_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given: the input
        final Book.BookBuilder bookBuilder = Book.getBuilder()
                .addIsbn(ISBN);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> bookBuilder.addIsbn(ISBN));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_price_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given: the input
        final Book.BookBuilder bookBuilder = Book.getBuilder()
                .addPrice(PRICE);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> bookBuilder.addPrice(PRICE));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_writers_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given: the input
        final Book.BookBuilder bookBuilder = Book.getBuilder()
                .addWriters(writers);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> bookBuilder.addWriters(writers));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_mainTitle_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given: the input
        final Book.BookBuilder bookBuilder = Book.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addIsbn(ISBN)
                .addPrice(PRICE)
                .addWriters(writers);
        bookBuilder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> bookBuilder.addMainTitle(MAIN_TITLE));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_subTitle_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given: the input
        final Book.BookBuilder bookBuilder = Book.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addIsbn(ISBN)
                .addPrice(PRICE)
                .addWriters(writers);
        bookBuilder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> bookBuilder.addSubTitle(SUB_TITLE));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_isbn_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given: the input
        final Book.BookBuilder bookBuilder = Book.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addIsbn(ISBN)
                .addPrice(PRICE)
                .addWriters(writers);
        bookBuilder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> bookBuilder.addIsbn(ISBN));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_price_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given: the input
        final Book.BookBuilder bookBuilder = Book.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addIsbn(ISBN)
                .addPrice(PRICE)
                .addWriters(writers);
        bookBuilder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> bookBuilder.addPrice(PRICE));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_writers_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given: the input
        final Book.BookBuilder bookBuilder = Book.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addIsbn(ISBN)
                .addPrice(PRICE)
                .addWriters(writers);
        bookBuilder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> bookBuilder.addWriters(writers));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given: the input
        final Book.BookBuilder bookBuilder = Book.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addIsbn(ISBN)
                .addPrice(PRICE)
                .addWriters(writers);
        bookBuilder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, bookBuilder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }

}
