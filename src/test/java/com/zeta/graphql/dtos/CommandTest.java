package com.zeta.graphql.dtos;

import com.zeta.graphql.exceptions.MandatoryValueException;
import com.zeta.graphql.exceptions.ZetaIllegalActionException;
import com.zeta.graphql.toolbox.TestErrorConstants;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.NullSource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CommandTest {

    private static List<ToBeCommandedBook> toBeCommandedBooks;
    private static Long BOOK = 1L;
    private static Integer NO_COPIES = 14;
    private static final Long CLIENT = 1L;

    @BeforeAll
    static void init() {
        final ToBeCommandedBook toBeCommandedBook = ToBeCommandedBook.getBuilder()
                .addBook(BOOK)
                .addNumberOfCopies(NO_COPIES)
                .build();

        toBeCommandedBooks = new ArrayList<>(){
            {
                add(toBeCommandedBook);
            }
        };
    }

    @Test
    void whe_all_fields_fed_correctly_the_should_be_ok() {
        // given: the input

        // when
        final Command command = Command.getBuilder()
                .addBooks(toBeCommandedBooks)
                .addClient(CLIENT)
                .build();

        // then
        assertNotNull(command);
        assertEquals(toBeCommandedBooks, command.getBooks());
        assertEquals(CLIENT, command.getClient());
    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_toBeCommandedBooks_fed_null_or_empty_then_should_throw_MandatoryValueException(final List<ToBeCommandedBook> booksToBeCommanded) {
        // given: the input and
        final Command.CommandBuilder builder = Command.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addBooks(booksToBeCommanded));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_client_fed_null_then_should_throw_MandatoryValueException(final Long client) {
        // given: the input and
        final Command.CommandBuilder builder = Command.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addClient(client));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_toBeCommandedBooks_not_fed_then_should_throw_MandatoryValueException() {
        // given: the input and
        final Command.CommandBuilder builder = Command.getBuilder()
                .addClient(CLIENT);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_client_not_fed_then_should_throw_MandatoryValueException() {
        // given: the input and
        final Command.CommandBuilder builder = Command.getBuilder()
                .addBooks(toBeCommandedBooks);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_toBeCommandedBooks_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given: the input and
        final Command.CommandBuilder builder = Command.getBuilder()
                .addBooks(toBeCommandedBooks);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBooks(toBeCommandedBooks));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_client_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given: the input and
        final Command.CommandBuilder builder = Command.getBuilder()
                .addClient(CLIENT);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addClient(CLIENT));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_toBeCommandedBooks_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given: the input and
        final Command.CommandBuilder builder = Command.getBuilder()
                .addBooks(toBeCommandedBooks)
                .addClient(CLIENT);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBooks(toBeCommandedBooks));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_client_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given: the input and
        final Command.CommandBuilder builder = Command.getBuilder()
                .addBooks(toBeCommandedBooks)
                .addClient(CLIENT);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addClient(CLIENT));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given: the input and
        final Command.CommandBuilder builder = Command.getBuilder()
                .addBooks(toBeCommandedBooks)
                .addClient(CLIENT);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }
}
