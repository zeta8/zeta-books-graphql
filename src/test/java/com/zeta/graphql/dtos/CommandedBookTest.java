package com.zeta.graphql.dtos;

import com.zeta.graphql.entities.BookEntity;
import com.zeta.graphql.exceptions.MandatoryValueException;
import com.zeta.graphql.exceptions.ZetaIllegalActionException;
import com.zeta.graphql.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class CommandedBookTest {

    private static final String MAIN_TITLE = "The Adapted mind";
    private static final String SUB_TITLE = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN = "0-19-506023-7";
    private static final BigDecimal PRICE = new BigDecimal("25.49");

    private static final BookEntity book = BookEntity.getBuilder()
            .addMainTitle(MAIN_TITLE)
            .addSubTitle(SUB_TITLE)
            .addIsbn(ISBN)
            .addPrice(PRICE)
            .build();
    private static final Integer NO_COPIES = 1;

    @Test
    void when_all_fields_fed_correctly_then_should_be_ok() {
        // given: the input

        // when
        final CommandedBook commandedBook = CommandedBook.getBuilder()
                .addBook(book)
                .addNumberOfCopies(NO_COPIES)
                .build();

        // then
        assertNotNull(commandedBook);
        assertEquals(book, commandedBook.getBook());
        assertEquals(NO_COPIES, commandedBook.getNumberOfCopies());
    }

    @Test
    void when_book_fed_null_then_should_throw_MandatoryValueException() {
        // given: the input and
        final CommandedBook.CommandedBookBuilder builder = CommandedBook.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addBook(null));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_numberOfCopies_fed_null_then_should_throw_MandatoryValueException() {
        // given: the input and
        final CommandedBook.CommandedBookBuilder builder = CommandedBook.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addNumberOfCopies(null));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_book_not_fed_then_should_throw_MandatoryValueException() {
        // given: the input and
        final CommandedBook.CommandedBookBuilder builder =
                CommandedBook.getBuilder()
                        .addNumberOfCopies(NO_COPIES);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_numberOfCopies_not_fed_then_should_throw_MandatoryValueException() {
        // given: the input and
        final CommandedBook.CommandedBookBuilder builder =
                CommandedBook.getBuilder()
                        .addBook(book);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_book_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given: the input and
        final CommandedBook.CommandedBookBuilder builder =
                CommandedBook.getBuilder()
                        .addBook(book);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBook(book));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_addNumberOfCopies_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given: the input and
        final CommandedBook.CommandedBookBuilder builder =
                CommandedBook.getBuilder()
                        .addNumberOfCopies(NO_COPIES);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () ->  builder.addNumberOfCopies(NO_COPIES));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_book_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given: the input and
        final CommandedBook.CommandedBookBuilder builder =
                CommandedBook.getBuilder()
                        .addBook(book)
                .addNumberOfCopies(NO_COPIES);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBook(book));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_numberOfCopies_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given: the input and
        final CommandedBook.CommandedBookBuilder builder =
                CommandedBook.getBuilder()
                        .addBook(book)
                        .addNumberOfCopies(NO_COPIES);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addNumberOfCopies(NO_COPIES));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given: the input and
        final CommandedBook.CommandedBookBuilder builder =
                CommandedBook.getBuilder()
                        .addBook(book)
                        .addNumberOfCopies(NO_COPIES);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }
}
