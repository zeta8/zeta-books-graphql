package com.zeta.graphql.dtos;

import com.zeta.graphql.enums.StatusEnum;
import com.zeta.graphql.exceptions.MandatoryValueException;
import com.zeta.graphql.exceptions.ZetaIllegalActionException;
import com.zeta.graphql.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.NullSource;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class PersonInformationTest {

    private static final String PERSON_CODE = "CD-1";
    private static final String FIRST_NAME = "Wolfgang";
    private static final String MIDDLE_NAME = "Amadeus";
    private static final String LAST_NAME = "MOZART";
    private static final LocalDate BIRTH_DATE = LocalDate.of(1756, 1, 27);
    private static final StatusEnum STATUS = StatusEnum.WRI;

    @Test
    void when_all_fields_fed_properly_then_should_be_ok() {
        // given: the input

        // when
        final PersonInformation build = PersonInformation.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(STATUS)
                .build();

        // then
        assertNotNull(build);
        assertEquals(PERSON_CODE, build.getPersonCode());
        assertEquals(FIRST_NAME, build.getFirstName());
        assertEquals(MIDDLE_NAME, build.getMiddleName());
        assertEquals(LAST_NAME, build.getLastName());
        assertEquals(BIRTH_DATE, build.getBirthDate());
        assertEquals(STATUS, build.getStatus());
    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_middleName_fed_null_or_empty_then_should_be_ok(final String middleName) {
        // given: the input

        // when
        final PersonInformation build = PersonInformation.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(middleName)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(STATUS)
                .build();

        // then
        assertNotNull(build);
        assertEquals(PERSON_CODE, build.getPersonCode());
        assertEquals(FIRST_NAME, build.getFirstName());
        assertNull(build.getMiddleName());
        assertEquals(LAST_NAME, build.getLastName());
        assertEquals(BIRTH_DATE, build.getBirthDate());
        assertEquals(STATUS, build.getStatus());
    }

    @Test
    void when_middleName_not_fed_then_should_be_ok() {
        // given: the input

        // when
        final PersonInformation build = PersonInformation.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(STATUS)
                .build();

        // then
        assertNotNull(build);
        assertEquals(PERSON_CODE, build.getPersonCode());
        assertEquals(FIRST_NAME, build.getFirstName());
        assertNull(build.getMiddleName());
        assertEquals(LAST_NAME, build.getLastName());
        assertEquals(BIRTH_DATE, build.getBirthDate());
        assertEquals(STATUS, build.getStatus());
    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_personCode_fed_null_or_empty_then_should_throw_MandatoryValueException(final String personCode) {
        // given
        final PersonInformation.PersonInformationBuilder builder = PersonInformation.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addPersonCode(personCode));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_firstName_fed_null_or_empty_then_should_throw_MandatoryValueException(final String firstName) {
        // given
        final PersonInformation.PersonInformationBuilder builder = PersonInformation.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addFirstName(firstName));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_lastName_fed_null_or_empty_then_should_throw_MandatoryValueException(final String firstName) {
        // given
        final PersonInformation.PersonInformationBuilder builder = PersonInformation.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addLastName(firstName));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_birthdate_fed_null_then_should_throw_MandatoryValueException(final LocalDate birthDate) {
        // given
        final PersonInformation.PersonInformationBuilder builder = PersonInformation.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addBirthDate(birthDate));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullSource
    void when_status_fed_null_then_should_throw_MandatoryValueException(final StatusEnum statusEnum) {
        // given
        final PersonInformation.PersonInformationBuilder builder = PersonInformation.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addStatus(statusEnum));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_personCode_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final PersonInformation.PersonInformationBuilder builder =
                PersonInformation.getBuilder()
                        .addFirstName(FIRST_NAME)
                        .addLastName(LAST_NAME)
                        .addBirthDate(BIRTH_DATE)
                        .addStatus(STATUS);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_firstName_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final PersonInformation.PersonInformationBuilder builder =
                PersonInformation.getBuilder()
                        .addPersonCode(PERSON_CODE)
                        .addLastName(LAST_NAME)
                        .addBirthDate(BIRTH_DATE)
                        .addStatus(STATUS);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_lastName_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final PersonInformation.PersonInformationBuilder builder =
                PersonInformation.getBuilder()
                        .addPersonCode(PERSON_CODE)
                        .addFirstName(FIRST_NAME)
                        .addBirthDate(BIRTH_DATE)
                        .addStatus(STATUS);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_birthDate_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final PersonInformation.PersonInformationBuilder builder =
                PersonInformation.getBuilder()
                        .addPersonCode(PERSON_CODE)
                        .addFirstName(FIRST_NAME)
                        .addLastName(LAST_NAME)
                        .addStatus(STATUS);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_status_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final PersonInformation.PersonInformationBuilder builder =
                PersonInformation.getBuilder()
                        .addPersonCode(PERSON_CODE)
                        .addFirstName(FIRST_NAME)
                        .addLastName(LAST_NAME)
                        .addBirthDate(BIRTH_DATE);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_personCode_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonInformation.PersonInformationBuilder builder =
                PersonInformation.getBuilder()
                        .addPersonCode(PERSON_CODE)
                        .addFirstName(FIRST_NAME)
                        .addLastName(LAST_NAME)
                        .addBirthDate(BIRTH_DATE)
                        .addStatus(STATUS);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addPersonCode(PERSON_CODE));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_firstName_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonInformation.PersonInformationBuilder builder =
                PersonInformation.getBuilder()
                        .addPersonCode(PERSON_CODE)
                        .addFirstName(FIRST_NAME)
                        .addLastName(LAST_NAME)
                        .addBirthDate(BIRTH_DATE)
                        .addStatus(STATUS);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addFirstName(FIRST_NAME));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_middleName_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonInformation.PersonInformationBuilder builder =
                PersonInformation.getBuilder()
                        .addPersonCode(PERSON_CODE)
                        .addFirstName(FIRST_NAME)
                        .addLastName(LAST_NAME)
                        .addBirthDate(BIRTH_DATE)
                        .addStatus(STATUS);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addMiddleName(MIDDLE_NAME));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_lastName_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonInformation.PersonInformationBuilder builder =
                PersonInformation.getBuilder()
                        .addPersonCode(PERSON_CODE)
                        .addFirstName(FIRST_NAME)
                        .addLastName(LAST_NAME)
                        .addBirthDate(BIRTH_DATE)
                        .addStatus(STATUS);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addLastName(FIRST_NAME));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_birthDate_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonInformation.PersonInformationBuilder builder =
                PersonInformation.getBuilder()
                        .addPersonCode(PERSON_CODE)
                        .addFirstName(FIRST_NAME)
                        .addLastName(LAST_NAME)
                        .addBirthDate(BIRTH_DATE)
                        .addStatus(STATUS);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBirthDate(BIRTH_DATE));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_status_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonInformation.PersonInformationBuilder builder =
                PersonInformation.getBuilder()
                        .addPersonCode(PERSON_CODE)
                        .addFirstName(FIRST_NAME)
                        .addLastName(LAST_NAME)
                        .addBirthDate(BIRTH_DATE)
                        .addStatus(STATUS);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addStatus(STATUS));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_personCode_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonInformation.PersonInformationBuilder builder = PersonInformation.getBuilder()
                .addPersonCode(PERSON_CODE);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addPersonCode(PERSON_CODE));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_firstname_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonInformation.PersonInformationBuilder builder = PersonInformation.getBuilder()
                .addFirstName(FIRST_NAME);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addFirstName(FIRST_NAME));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_middleName_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonInformation.PersonInformationBuilder builder = PersonInformation.getBuilder()
                .addMiddleName(MIDDLE_NAME);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addMiddleName(MIDDLE_NAME));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_lastName_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonInformation.PersonInformationBuilder builder = PersonInformation.getBuilder()
                .addLastName(LAST_NAME);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addLastName(LAST_NAME));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_birthDate_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonInformation.PersonInformationBuilder builder = PersonInformation.getBuilder()
                .addBirthDate(BIRTH_DATE);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBirthDate(BIRTH_DATE));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_status_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonInformation.PersonInformationBuilder builder = PersonInformation.getBuilder()
                .addStatus(STATUS);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addStatus(STATUS));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final PersonInformation.PersonInformationBuilder builder = PersonInformation.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(STATUS);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }
}
