package com.zeta.graphql.dtos;

import com.zeta.graphql.exceptions.MandatoryValueException;
import com.zeta.graphql.exceptions.ZetaIllegalActionException;
import com.zeta.graphql.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;

import static org.junit.jupiter.api.Assertions.*;

class StatusDtoTest {

    private static final String CODE = "ADM";
    private static final String LABEL = "Administrator";
    private static final String DESCRIPTION = "The administrator of Zeta";

    @Test
    void when_all_fields_fed_properly_then_should_be_ok() {
        // given: the input

        // when
        StatusDto statusDto = StatusDto.getBuilder()
                .addCode(CODE)
                .addLabel(LABEL)
                .addDescription(DESCRIPTION)
                .build();

        // then
        assertNotNull(statusDto);
        assertEquals(CODE, statusDto.getCode());
        assertEquals(LABEL, statusDto.getLabel());
        assertEquals(DESCRIPTION, statusDto.getDescription());
    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_code_fed_null_or_empty_then_should_throw_MandatoryValueException(final String code) {
        // given:
        final StatusDto.StatusDtoBuilder builder = StatusDto.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addCode(code));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_label_fed_null_or_empty_then_should_throw_MandatoryValueException(final String label) {
        // given:
        final StatusDto.StatusDtoBuilder builder = StatusDto.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addLabel(label));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void when_description_fed_null_or_empty_then_should_throw_MandatoryValueException(final String description) {
        // given
        final StatusDto.StatusDtoBuilder builder = StatusDto.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addDescription(description));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_code_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final StatusDto.StatusDtoBuilder builder = StatusDto.getBuilder()
                .addLabel(LABEL)
                .addDescription(DESCRIPTION);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_label_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final StatusDto.StatusDtoBuilder builder = StatusDto.getBuilder()
                .addCode(CODE)
                .addDescription(DESCRIPTION);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_description_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final StatusDto.StatusDtoBuilder builder = StatusDto.getBuilder()
                .addCode(CODE)
                .addLabel(LABEL);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_code_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final StatusDto.StatusDtoBuilder builder = StatusDto.getBuilder()
                .addCode(CODE);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addCode(CODE));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_label_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final StatusDto.StatusDtoBuilder builder = StatusDto.getBuilder()
                .addLabel(LABEL);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addLabel(LABEL));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_description_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final StatusDto.StatusDtoBuilder builder = StatusDto.getBuilder()
                .addDescription(LABEL);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addDescription(DESCRIPTION));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_code_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final StatusDto.StatusDtoBuilder builder = StatusDto.getBuilder()
                .addCode(CODE)
                .addLabel(LABEL)
                .addDescription(DESCRIPTION);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addCode(CODE));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_label_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final StatusDto.StatusDtoBuilder builder = StatusDto.getBuilder()
                .addCode(CODE)
                .addLabel(LABEL)
                .addDescription(DESCRIPTION);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addLabel(LABEL));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_description_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final StatusDto.StatusDtoBuilder builder = StatusDto.getBuilder()
                .addCode(CODE)
                .addLabel(LABEL)
                .addDescription(DESCRIPTION);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addDescription(DESCRIPTION));

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final StatusDto.StatusDtoBuilder builder = StatusDto.getBuilder()
                .addCode(CODE)
                .addLabel(LABEL)
                .addDescription(DESCRIPTION);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }
}