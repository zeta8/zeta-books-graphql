package com.zeta.graphql.dtos;

import com.zeta.graphql.exceptions.MandatoryValueException;
import com.zeta.graphql.exceptions.ZetaIllegalActionException;
import com.zeta.graphql.toolbox.TestErrorConstants;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class ToBeCommandedBookTest {

    private static final Long BOOK = 1L;
    private static final Integer NO_COPIES = 1;

    @Test
    void when_all_fields_fed_properly_then_should_be_ok() {
        // given: the input

        // when
        final ToBeCommandedBook toBeCommandedBook = ToBeCommandedBook.getBuilder()
                .addBook(BOOK)
                .addNumberOfCopies(NO_COPIES)
                .build();

        // then
        assertNotNull(toBeCommandedBook);
        assertEquals(BOOK, toBeCommandedBook.getBook());
        assertEquals(NO_COPIES, toBeCommandedBook.getNumberOfCopies());
    }

    @Test
    void when_book_fed_null_then_should_throw_MandatoryValueException() {
        // given
        final ToBeCommandedBook.ToBeCommandedBookBuilder builder = ToBeCommandedBook.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addBook(null));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_numberOfCopies_fed_null_then_should_throw_MandatoryValueException() {
        // given
        final ToBeCommandedBook.ToBeCommandedBookBuilder builder = ToBeCommandedBook.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addNumberOfCopies(null));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_book_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final ToBeCommandedBook.ToBeCommandedBookBuilder builder =
                ToBeCommandedBook.getBuilder()
                        .addNumberOfCopies(NO_COPIES);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_numberOfCopies_not_fed_then_should_throw_MandatoryValueException() {
        // given
        final ToBeCommandedBook.ToBeCommandedBookBuilder builder =
                ToBeCommandedBook.getBuilder()
                        .addBook(BOOK);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_book_fed_twice_then_should_throw_ZetaIllegalActionException() {
        // given
        final ToBeCommandedBook.ToBeCommandedBookBuilder builder =
                ToBeCommandedBook.getBuilder()
                        .addBook(BOOK);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBook(BOOK));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_numberOfCopies_fed_twice_then_should_throw_MandatoryValueException() {
        // given
        final ToBeCommandedBook.ToBeCommandedBookBuilder builder =
                ToBeCommandedBook.getBuilder()
                        .addNumberOfCopies(NO_COPIES);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addNumberOfCopies(NO_COPIES));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_book_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given
        final ToBeCommandedBook.ToBeCommandedBookBuilder builder =
                ToBeCommandedBook.getBuilder()
                        .addBook(BOOK)
                        .addNumberOfCopies(NO_COPIES);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBook(BOOK));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_numberOfCopies_fed_after_build_then_should_throw_MandatoryValueException() {
        // given
        final ToBeCommandedBook.ToBeCommandedBookBuilder builder =
                ToBeCommandedBook.getBuilder()
                        .addBook(BOOK)
                        .addNumberOfCopies(NO_COPIES);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addNumberOfCopies(NO_COPIES));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_MandatoryValueException() {
        // given
        final ToBeCommandedBook.ToBeCommandedBookBuilder builder =
                ToBeCommandedBook.getBuilder()
                        .addBook(BOOK)
                        .addNumberOfCopies(NO_COPIES);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }
}