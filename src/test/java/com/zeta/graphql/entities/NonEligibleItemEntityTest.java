package com.zeta.graphql.entities;

import com.zeta.graphql.enums.NonEligibilityReasonEnum;
import com.zeta.graphql.exceptions.IncoherentValueException;
import com.zeta.graphql.exceptions.InvalidValueException;
import com.zeta.graphql.exceptions.MandatoryValueException;
import com.zeta.graphql.exceptions.ZetaIllegalActionException;
import com.zeta.graphql.toolbox.TestErrorConstants;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class NonEligibleItemEntityTest {

    private static final String MAIN_TITLE = "The Adapted mind";
    private static final String SUB_TITLE = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN = "0-19-506023-7";
    private static final BigDecimal PRICE = new BigDecimal("25.49");

    private static final String PERSON_CODE = "CD-1";
    private static final String FIRST_NAME = "Wolfgang";
    private static final String MIDDLE_NAME = "Amadeus";
    private static final String LAST_NAME = "MOZART";
    private static final LocalDate BIRTH_DATE = LocalDate.of(1756, 1, 27);

    private static final NonEligibilityReasonEnum notEnoughCopiesReason = NonEligibilityReasonEnum.NOT_ENOUGH_COPIES;
    private static final NonEligibilityReasonEnum notFoundReason = NonEligibilityReasonEnum.NOT_FOUND;
    private static NonEligibilityReasonEntity nonEligibilityNotEnough;
    private static NonEligibilityReasonEntity nonEligibilityNotFound;
    private static final Long nonExistentBook = 1L;
    private static BookEntity book;
    private static CommandEntity command;
    private static PersonEntity client;

    @BeforeAll
    static void init() {
        nonEligibilityNotEnough = NonEligibilityReasonEntity.getBuilder()
                .addCode(notEnoughCopiesReason.name())
                .addLabel(notEnoughCopiesReason.getLabel())
                .addDescription(notEnoughCopiesReason.getDescription())
                .build();

        nonEligibilityNotFound = NonEligibilityReasonEntity.getBuilder()
                .addCode(notFoundReason.name())
                .addLabel(notFoundReason.getLabel())
                .addDescription(notEnoughCopiesReason.getDescription())
                .build();

        book = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addIsbn(ISBN)
                .addPrice(PRICE)
                .build();

        client = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .build();

        command = CommandEntity.getBuilder()
                .addClient(client)
                .addTotalPrice(PRICE)
                .build();
    }

    @Test
    void when_nonExistentBook_not_fed_but_book_fed_with_not_enough_reason_should_be_ok() {
        // given: The input

        // when
        final NonEligibleItemEntity nonEligibleItem = NonEligibleItemEntity.getBuilder()
                .addReason(nonEligibilityNotEnough)
                .addBook(book)
                .addCommand(command)
                .build();

        // then
        assertNotNull(nonEligibleItem);
        assertEquals(nonEligibilityNotEnough, nonEligibleItem.getReason());
        assertNull(nonEligibleItem.getNonExistingBook());
        assertEquals(book, nonEligibleItem.getBook());
        assertEquals(command, nonEligibleItem.getCommand());
    }

    @Test
    void when_nonExistentBook_fed_null_but_book_fed_with_not_enough_reason_should_be_ok() {
        // given: The input

        // when
        final NonEligibleItemEntity nonEligibleItem = NonEligibleItemEntity.getBuilder()
                .addReason(nonEligibilityNotEnough)
                .addNonExistingBook(null)
                .addBook(book)
                .addCommand(command)
                .build();

        // then
        assertNotNull(nonEligibleItem);
        assertEquals(nonEligibilityNotEnough, nonEligibleItem.getReason());
        assertNull(nonEligibleItem.getNonExistingBook());
        assertEquals(book, nonEligibleItem.getBook());
        assertEquals(command, nonEligibleItem.getCommand());
    }

    @Test
    void when_nonExistentBook_fed_with_not_found_reason_but_book_not_fed_should_be_ok() {
        // given: The input

        // when
        final NonEligibleItemEntity nonEligibleItem = NonEligibleItemEntity.getBuilder()
                .addReason(nonEligibilityNotFound)
                .addNonExistingBook(nonExistentBook)
                .addCommand(command)
                .build();

        // then
        assertNotNull(nonEligibleItem);
        assertEquals(nonEligibilityNotFound, nonEligibleItem.getReason());
        assertEquals(nonExistentBook, nonEligibleItem.getNonExistingBook());
        assertNull(nonEligibleItem.getBook());
        assertEquals(command, nonEligibleItem.getCommand());
    }

    @Test
    void when_both_nonExistent_book_and_book_fed_with_not_enough_reason_then_should_throw_IncoherentValueException() {
        // given: The input and
        final NonEligibleItemEntity.NonEligibleItemEntityBuilder builder = NonEligibleItemEntity.getBuilder()
                .addReason(nonEligibilityNotEnough)
                .addNonExistingBook(nonExistentBook)
                .addBook(book)
                .addCommand(command);

        // when
        final IncoherentValueException exception = assertThrows(
                IncoherentValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NON_ACCUMULATIVE_FIELD_ERR));
    }

    @Test
    void when_nonExistentBook_fed_with_not_found_reason_but_book_fed_null_should_be_ok() {
        // given: The input

        // when
        final NonEligibleItemEntity nonEligibleItem = NonEligibleItemEntity.getBuilder()
                .addReason(nonEligibilityNotFound)
                .addNonExistingBook(nonExistentBook)
                .addBook(null)
                .addCommand(command)
                .build();

        // then
        assertNotNull(nonEligibleItem);
        assertEquals(nonEligibilityNotFound, nonEligibleItem.getReason());
        assertEquals(nonExistentBook, nonEligibleItem.getNonExistingBook());
        assertNull(nonEligibleItem.getBook());
        assertEquals(command, nonEligibleItem.getCommand());
    }


    @Test
    void when_nonExistentBook_fed_with_not_enough_reason_but_book_not_fed_should_throw_IncoherentValueException() {
        // given: The input
        final NonEligibleItemEntity.NonEligibleItemEntityBuilder builder =
                NonEligibleItemEntity.getBuilder()
                        .addReason(nonEligibilityNotEnough)
                        .addNonExistingBook(nonExistentBook)
                        .addCommand(command);

        // when
        final IncoherentValueException exception = assertThrows(
                IncoherentValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.COUPLED_FIELDS_MSG));
    }

    @Test
    void when_nonExistentBook_fed_with_not_enough_reason_but_book_fed_null_should_throw_IncoherentValueException() {
        // given: The input
        final NonEligibleItemEntity.NonEligibleItemEntityBuilder builder =
                NonEligibleItemEntity.getBuilder()
                        .addReason(nonEligibilityNotEnough)
                        .addNonExistingBook(nonExistentBook)
                        .addBook(null)
                        .addCommand(command);

        // when
        final IncoherentValueException exception = assertThrows(
                IncoherentValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.COUPLED_FIELDS_MSG));
    }

    @Test
    void when_nonExistentBook_not_fed_but_book_fed_with_notFound_reason_then_should_throw_IncoherentValueException() {
        // given: The input
        final NonEligibleItemEntity.NonEligibleItemEntityBuilder builder =
                NonEligibleItemEntity.getBuilder()
                        .addReason(nonEligibilityNotFound)
                        .addBook(book)
                        .addCommand(command);

        // when
        final IncoherentValueException exception = assertThrows(
                IncoherentValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.COUPLED_FIELDS_MSG));
    }

    @Test
    void when_nonExistentBook_fed_null_but_book_fed_with_notFound_reason_then_should_throw_IncoherentValueException() {
        // given: The input
        final NonEligibleItemEntity.NonEligibleItemEntityBuilder builder =
                NonEligibleItemEntity.getBuilder()
                        .addReason(nonEligibilityNotFound)
                        .addNonExistingBook(null)
                        .addBook(book)
                        .addCommand(command);

        // when
        final IncoherentValueException exception = assertThrows(
                IncoherentValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.COUPLED_FIELDS_MSG));
    }

    @Test
    void when_reason_not_fed_then_then_should_throw_MandatoryValueException() {
        // given: The input and
        final NonEligibleItemEntity.NonEligibleItemEntityBuilder builder = NonEligibleItemEntity.getBuilder()
                .addNonExistingBook(nonExistentBook)
                .addBook(null)
                .addCommand(command);

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_reason_fed_null_then_then_should_throw_MandatoryValueException() {
        // given: The input and
        final NonEligibleItemEntity.NonEligibleItemEntityBuilder builder =
                NonEligibleItemEntity.getBuilder();

        // when
        final MandatoryValueException exception = assertThrows(
                MandatoryValueException.class, () -> builder.addReason(null));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.MANDATORY_VALUE_MSG));
    }

    @Test
    void when_neither_nonExistentBook_nor_book_not_fed_and_reason_notFound_then_should_throw_IncoherentValueException() {
        // given: The input and
        final NonEligibleItemEntity.NonEligibleItemEntityBuilder builder = NonEligibleItemEntity.getBuilder()
                .addReason(nonEligibilityNotFound)
                .addCommand(command);

        // when
        final InvalidValueException exception = assertThrows(
                InvalidValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ONE_FILED_MANDATORY));
    }

    @ParameterizedTest
    @MethodSource({"com.zeta.graphql.toolbox.parametrized.NonEligibilityParams#bothReasons"})
    void when_neither_nonExistentBook_nor_book_not_fed_and_reason_notEnough_or_notFound_then_should_throw_IncoherentValueException(final NonEligibilityReasonEntity reason) {
        // given: The input and
        final NonEligibleItemEntity.NonEligibleItemEntityBuilder builder = NonEligibleItemEntity.getBuilder()
                .addReason(reason)
                .addCommand(command);

        // when
        final InvalidValueException exception = assertThrows(
                InvalidValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ONE_FILED_MANDATORY));
    }


    @ParameterizedTest
    @MethodSource({"com.zeta.graphql.toolbox.parametrized.NonEligibilityParams#bothReasons"})
    void when_both_nonExistentBook_and_book_fed_null_and_reason_notFound_or_noEnough_then_should_throw_IncoherentValueException(final NonEligibilityReasonEntity reason) {
        // given: The input and
        final NonEligibleItemEntity.NonEligibleItemEntityBuilder builder = NonEligibleItemEntity.getBuilder()
                .addReason(reason)
                .addNonExistingBook(null)
                .addBook(null)
                .addCommand(command);

        // when
        final InvalidValueException exception = assertThrows(
                InvalidValueException.class, builder::build);

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ONE_FILED_MANDATORY));
    }

    @ParameterizedTest
    @MethodSource({"com.zeta.graphql.toolbox.parametrized.NonEligibilityParams#bothReasons"})
    void when_reason_fed_twice_then_should_throw_ZetaIllegalActionException(final NonEligibilityReasonEntity reason) {
        // given
        final NonEligibleItemEntity.NonEligibleItemEntityBuilder builder =
                NonEligibleItemEntity.getBuilder()
                        .addReason(reason);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addReason(reason));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_book_fed_twice_then_should_throw_ZetaIllegalActionException() {
        //given
        final NonEligibleItemEntity.NonEligibleItemEntityBuilder builder =
                NonEligibleItemEntity.getBuilder()
                        .addBook(book);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBook(book));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_nonExistentBook_fed_twice_then_should_throw_ZetaIllegalActionException() {
        //given
        final NonEligibleItemEntity.NonEligibleItemEntityBuilder builder =
                NonEligibleItemEntity.getBuilder()
                        .addNonExistingBook(nonExistentBook);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addNonExistingBook(12L));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_command_fed_twice_then_should_throw_ZetaIllegalActionException() {
        //given
        final NonEligibleItemEntity.NonEligibleItemEntityBuilder builder =
                NonEligibleItemEntity.getBuilder()
                        .addCommand(command);

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addCommand(command));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ALREADY_FED_MSG));
    }

    @Test
    void when_reason_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given: The input
        final NonEligibleItemEntity.NonEligibleItemEntityBuilder builder =
                NonEligibleItemEntity.getBuilder()
                        .addReason(nonEligibilityNotFound)
                        .addNonExistingBook(nonExistentBook)
                        .addBook(null)
                        .addCommand(command);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addReason(nonEligibilityNotFound));

        // then
        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_nonExistingBook_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given: The input
        final NonEligibleItemEntity.NonEligibleItemEntityBuilder builder =
                NonEligibleItemEntity.getBuilder()
                        .addReason(nonEligibilityNotFound)
                        .addNonExistingBook(nonExistentBook)
                        .addBook(null)
                        .addCommand(command);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addNonExistingBook(nonExistentBook));

        // then
        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_book_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given: The input
        final NonEligibleItemEntity.NonEligibleItemEntityBuilder builder =
                NonEligibleItemEntity.getBuilder()
                        .addReason(nonEligibilityNotFound)
                        .addNonExistingBook(nonExistentBook)
                        .addBook(null)
                        .addCommand(command);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addBook(book));

        // then
        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_command_fed_after_build_then_should_throw_ZetaIllegalActionException() {
        // given: The input
        final NonEligibleItemEntity.NonEligibleItemEntityBuilder builder =
                NonEligibleItemEntity.getBuilder()
                        .addReason(nonEligibilityNotFound)
                        .addNonExistingBook(nonExistentBook)
                        .addBook(null)
                        .addCommand(command);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, () -> builder.addCommand(command));

        // then
        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.FED_AFTER_INSTANTIATION_MSG));
    }

    @Test
    void when_build_twice_then_should_throw_ZetaIllegalActionException() {
        // given: The input
        final NonEligibleItemEntity.NonEligibleItemEntityBuilder builder =
                NonEligibleItemEntity.getBuilder()
                        .addReason(nonEligibilityNotFound)
                        .addNonExistingBook(nonExistentBook)
                        .addBook(null)
                        .addCommand(command);
        builder.build();

        // when
        final ZetaIllegalActionException exception = assertThrows(
                ZetaIllegalActionException.class, builder::build);

        // then
        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.DUAL_BUILD_MSG));
    }


}

