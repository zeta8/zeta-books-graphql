package com.zeta.graphql.services;

import com.zeta.graphql.GraphqlApplication;
import com.zeta.graphql.dtos.Book;
import com.zeta.graphql.entities.BookEntity;
import com.zeta.graphql.entities.PersonEntity;
import com.zeta.graphql.entities.StockEntity;
import com.zeta.graphql.exceptions.ResourceNotFoundException;
import com.zeta.graphql.repositories.BookWriterRepository;
import com.zeta.graphql.repositories.BooksRepository;
import com.zeta.graphql.repositories.PersonRepository;
import com.zeta.graphql.repositories.StockRepository;
import com.zeta.graphql.services.interfaces.BookService;
import com.zeta.graphql.toolbox.TestErrorConstants;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {GraphqlApplication.class, BookServiceTestsIT.Configuration.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BookServiceTestsIT {

    @Autowired
    private BookService bookService;

    private static final String MAIN_TITLE = "The Adapted mind";
    private static final String SUB_TITLE = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN = "0-19-506023-7";
    private static final BigDecimal PRICE = new BigDecimal("25.49");
    private static final String ISBN_NOT = "ahfoarha";

    private static final String PERSON_CODE_1 = "CD-1";
    private static final String FIRST_NAME_1 = "Wolfgang";
    private static final String MIDDLE_NAME_1 = "Amadeus";
    private static final String LAST_NAME_1 = "MOZART";
    private static final LocalDate BIRTH_DATE_1 = LocalDate.of(1756, 1, 27);


    private static final String PERSON_CODE_2 = "CD-2";
    private static final String FIRST_NAME_2 = "Leda";
    private static final String LAST_NAME_2 = "Cosmides";
    private static final LocalDate BIRTH_DATE_2 = LocalDate.of(1957, 5, 9);

    private static StockEntity stockEntity;
    private static List<StockEntity> stockEntityList;
    private static BookEntity bookEntity;
    private static BookEntity nonExistingBookEntityBeforeSave;
    private static BookEntity getNonExistingBookEntityAfterSave;
    private static Book book;
    private static Book nonExistingBook;
    private static Book nonExistingBookWithNonExistingWriter;
    private static final Long FIRST_WRITER_ID = 1L;
    private static final Long SECOND_WRITER_ID = 2L;
    private static PersonEntity firstWriter;
    private static PersonEntity secondWriter;
    private static List<Long> existingWriters = new ArrayList<>() {
        {
            add(FIRST_WRITER_ID);
            add(SECOND_WRITER_ID);
        }
    };

    private static List<Long> nonExistingWriter = new ArrayList<>() {
        {
            add(FIRST_WRITER_ID);
            add(-1L);
        }
    };

    private static final Integer existingNumberOfCopiesInStock = 1;


    @BeforeAll
    static void init() {
        book = Book.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addIsbn(ISBN)
                .addPrice(PRICE)
                .addWriters(existingWriters)
                .build();

        nonExistingBook = Book.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addIsbn(ISBN_NOT)
                .addPrice(PRICE)
                .addWriters(existingWriters)
                .build();

        nonExistingBookWithNonExistingWriter =
                Book.getBuilder()
                        .addMainTitle(MAIN_TITLE)
                        .addSubTitle(SUB_TITLE)
                        .addIsbn(ISBN_NOT)
                        .addPrice(PRICE)
                        .addWriters(nonExistingWriter)
                        .build();

        bookEntity = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addIsbn(ISBN)
                .addPrice(PRICE)
                .build();

        final Class existingBookClass = bookEntity.getClass();
        try {
            final Field f = existingBookClass.getDeclaredField("id");
            f.setAccessible(true);
            f.set(bookEntity, 1L);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        stockEntity = StockEntity.getBuilder()
                .addBookEntity(bookEntity)
                .addNumberOfCopies(existingNumberOfCopiesInStock)
                .build();
        stockEntityList = new ArrayList<>() {
            {
                add(stockEntity);
            }
        };

        firstWriter = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE_1)
                .addFirstName(FIRST_NAME_1)
                .addMiddleName(MIDDLE_NAME_1)
                .addLastName(LAST_NAME_1)
                .addBirthDate(BIRTH_DATE_1)
                .build();

        secondWriter = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE_2)
                .addFirstName(FIRST_NAME_2)
                .addLastName(LAST_NAME_2)
                .addBirthDate(BIRTH_DATE_2)
                .build();

        nonExistingBookEntityBeforeSave = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addIsbn(ISBN_NOT)
                .addPrice(PRICE)
                .build();

        getNonExistingBookEntityAfterSave = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addIsbn(ISBN_NOT)
                .addPrice(PRICE)
                .build();

        final Class bookAfterSave = getNonExistingBookEntityAfterSave.getClass();
        try {
            final Field f = bookAfterSave.getDeclaredField("id");
            f.setAccessible(true);
            f.set(getNonExistingBookEntityAfterSave, 45L);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            ;
        }
    }

    @Test
    void when_finding_book_with_nonExisting_id_then_should_throw_ResourceNotFoundException() {
        // given: nonExisting book id
        final Long nonExistingId = -1L;

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> bookService.findBookById(nonExistingId));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @Test
    void when_adding_book_and_book_present_in_the_stock_then_should_increase_the_number_of_copies_by_one() {
        // given: a book already present in the stock

        // when
        final BookEntity bookEntity = bookService.addBook(book);

        // then
        assertNotNull(bookEntity);
        assertEquals(MAIN_TITLE, bookEntity.getMainTitle());
        assertEquals(SUB_TITLE, bookEntity.getSubTitle());
        assertEquals(ISBN, bookEntity.getIsbn());
        assertEquals(PRICE, bookEntity.getPrice());
        assertNotNull(bookEntity.getId());
        assertEquals(existingNumberOfCopiesInStock + 1, stockEntity.getNumberOfCopies());
    }

    @Test
    void when_adding_nonExisting_book_in_the_stock_with_existing_writers_then_should_be_Ok() {
        // given: a book not in the stock with a list of exiting writers

        // when
        final BookEntity bookEntity = bookService.addBook(nonExistingBook);

        // then
        assertNotNull(bookEntity);
        assertNotNull(bookEntity.getIsbn());
        assertEquals(ISBN_NOT, bookEntity.getIsbn());
    }

    @Test
    void when_adding_book_with_nonExisting_writer_then_should_throw_ResourceNotFoundException() {
        // given: a book with non existing writer

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> bookService.addBook(nonExistingBookWithNonExistingWriter));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @TestConfiguration
    static class Configuration {
        @Primary
        @Bean
        BookService bookService() {
            final BooksRepository booksRepository = mock(BooksRepository.class);
            final BookWriterRepository bookWriterRepository = mock(BookWriterRepository.class);
            final PersonRepository personRepository = mock(PersonRepository.class);
            final StockRepository stockRepository = mock(StockRepository.class);

            when(stockRepository.findAll()).thenReturn(stockEntityList);

            when(booksRepository.save(nonExistingBookEntityBeforeSave)).thenReturn(getNonExistingBookEntityAfterSave);

            when(personRepository.findById(FIRST_WRITER_ID)).thenReturn(Optional.of(firstWriter));
            when(personRepository.findById(SECOND_WRITER_ID)).thenReturn(Optional.of(secondWriter));

            return new BookServiceImpl(booksRepository, bookWriterRepository, personRepository, stockRepository);
        }
    }
}
