package com.zeta.graphql.services;

import com.zeta.graphql.GraphqlApplication;
import com.zeta.graphql.entities.BookEntity;
import com.zeta.graphql.entities.BookWriterEntity;
import com.zeta.graphql.entities.PersonEntity;
import com.zeta.graphql.exceptions.ResourceNotFoundException;
import com.zeta.graphql.repositories.BookWriterRepository;
import com.zeta.graphql.services.interfaces.BookWriterService;
import com.zeta.graphql.toolbox.TestErrorConstants;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {GraphqlApplication.class, BookWriterServiceTestsIT.Configuration.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BookWriterServiceTestsIT {

    @Autowired
    private BookWriterService bookWriterService;

    private static final String MAIN_TITLE = "The Adapted mind";
    private static final String SUB_TITLE = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN = "0-19-506023-7";
    private static final BigDecimal PRICE = new BigDecimal("25.49");

    private static final String PERSON_CODE_1 = "CD-1";
    private static final String FIRST_NAME_1 = "Wolfgang";
    private static final String MIDDLE_NAME_1 = "Amadeus";
    private static final String LAST_NAME_1 = "MOZART";
    private static final LocalDate BIRTH_DATE_1 = LocalDate.of(1756, 1, 27);


    private static final String PERSON_CODE_2 = "CD-2";
    private static final String FIRST_NAME_2 = "Leda";
    private static final String LAST_NAME_2 = "Cosmides";
    private static final LocalDate BIRTH_DATE_2 = LocalDate.of(1957, 5, 9);


    private static BookEntity book;
    private static BookEntity nonExistingBook;
    private static List<BookWriterEntity> writers;

    @BeforeAll
    static void init() {
        final Long existingBookId = 1L;
        final Long nonExistingBookId = 2L;

        book = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addIsbn(ISBN)
                .addPrice(PRICE)
                .build();

        final Class bookClass = book.getClass();
        try {
            final Field f = bookClass.getDeclaredField("id");
            f.setAccessible(true);
            f.set(book, existingBookId);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        nonExistingBook = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE)
                .addSubTitle(SUB_TITLE)
                .addIsbn(ISBN)
                .addPrice(PRICE)
                .build();

        final Class nonExistingBookClass = nonExistingBook.getClass();
        try {
            final Field f = nonExistingBookClass.getDeclaredField("id");
            f.setAccessible(true);
            f.set(nonExistingBook, nonExistingBookId);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }


        final PersonEntity writer1 = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE_1)
                .addFirstName(FIRST_NAME_1)
                .addMiddleName(MIDDLE_NAME_1)
                .addLastName(LAST_NAME_1)
                .addBirthDate(BIRTH_DATE_1)
                .build();

        final BookWriterEntity bookWriter1 = BookWriterEntity.getBuilder()
                .addWriter(writer1)
                .addBook(book)
                .build();

        final PersonEntity writer2 = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE_2)
                .addFirstName(FIRST_NAME_2)
                .addLastName(LAST_NAME_2)
                .addBirthDate(BIRTH_DATE_2)
                .build();

        final BookWriterEntity bookWriter2 = BookWriterEntity.getBuilder()
                .addWriter(writer2)
                .addBook(book)
                .build();

        writers = new ArrayList<>(){
            {
                add(bookWriter1);
                add(bookWriter2);
            }
        };
    }

    @Test
    void when_given_valid_book_entity_then_should_return_the_list_of_writers() {
        // given: the valid bookEntity

        // when
        List<PersonEntity> writerByBook = bookWriterService.findWriterByBook(book);

        assertNotNull(writerByBook);
        assertFalse(writerByBook.isEmpty());
    }

    @Test
    void when_bookEntity_not_found_then_should_throw_ResourceNotFoundException() {
        // given: a book with one nonExisting writer

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> bookWriterService.findWriterByBook(nonExistingBook));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @TestConfiguration
    static class Configuration {
        @Primary
        @Bean
        BookWriterService bookWriterService() {
            final BookWriterRepository bookWriterRepository = mock(BookWriterRepository.class);

            when(bookWriterRepository.findByBookEntity(book)).thenReturn(Optional.of(writers));

            return new BookWriterServiceImpl(bookWriterRepository);
        }
    }
}
