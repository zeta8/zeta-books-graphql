package com.zeta.graphql.services;

import com.zeta.graphql.GraphqlApplication;
import com.zeta.graphql.dtos.Command;
import com.zeta.graphql.dtos.CommandedBook;
import com.zeta.graphql.dtos.ToBeCommandedBook;
import com.zeta.graphql.entities.*;
import com.zeta.graphql.enums.NonEligibilityReasonEnum;
import com.zeta.graphql.exceptions.ResourceNotFoundException;
import com.zeta.graphql.exceptions.ZetaIllegalStateException;
import com.zeta.graphql.repositories.*;
import com.zeta.graphql.services.interfaces.CommandService;
import com.zeta.graphql.toolbox.TestErrorConstants;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {GraphqlApplication.class, CommandServiceTestsIT.Configuration.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CommandServiceTestsIT {

    @Autowired
    private CommandService commandService;


    private static final String PERSON_CODE = "CD-1";
    private static final String FIRST_NAME = "Wolfgang";
    private static final String MIDDLE_NAME = "Amadeus";
    private static final String LAST_NAME = "MOZART";
    private static final LocalDate BIRTH_DATE = LocalDate.of(1756, 1, 27);
    private static PersonEntity client;
    private static final Long CLIENT_ID = 41L;

    private static final String MAIN_TITLE_1 = "The Adapted mind";
    private static final String SUB_TITLE_1 = "Evolutionary Psychology and the Generation of Culture";
    private static final String ISBN_1 = "0-19-506023-7";
    private static final BigDecimal PRICE_1 = new BigDecimal("25.49");
    private static final Long BOOK_ID_1 = 101L;

    private static final String MAIN_TITLE_2 = "The Moral Animal";
    private static final String SUB_TITLE_2 = "Why We Are the Way We Are";
    private static final String ISBN_2 = "0-19-506023-7";
    private static final BigDecimal PRICE_2 = new BigDecimal("9.00");
    private static final Long BOOK_ID_2 = 102L;

    private static final String MAIN_TITLE_3 = "Why Race Matters";
    private static final String ISBN_3 = "978-0-9838910-3-1";
    private static final BigDecimal PRICE_3 = new BigDecimal("19.95");
    private static final Long BOOK_ID_3 = 103L;


    private static Command allEligibleCommand;
    private static Command semiEligibleCommand;
    private static Command nonEligibleCommand;
    private static Command clientNotFoundCommand;

    private static StockEntity eligibleStockEntity1;
    private static final Long STOCK_ID_1 = 201L;
    private static StockEntity eligibleStockEntity2;
    private static final Long STOCK_ID_2 = 202L;
    private static StockEntity notEnoughStockEntity;
    private static final Long STOCK_ID_3 = 203L;

    private static ToBeCommandedBook eligibleBook_1;
    private static BookEntity eligibleBookEntity_1;
    private static ToBeCommandedBook eligibleBook_2;
    private static BookEntity eligibleBookEntity_2;
    private static BookEntity notEnoughBookEntity;

    private static final Integer ELIGIBLE_NO_COPIES_1 = 12;
    private static final Integer ELIGIBLE_NO_COPIES_2 = 5;

    private static NonEligibilityReasonEntity notEnoughReasonEntity;
    private static NonEligibilityReasonEntity notFoundReasonEntity;

    private static CommandEntity commandEntityBeforeSave;
    private static CommandEntity commandEntityAfterSave;
    private static final BigDecimal TOTAL_PRICE = new BigDecimal("950.88");
    private static final Long COMMAND_ID = 300L;

    private static List<CommandBookEntity> commandBookEntityList;

    @BeforeAll
    static void ini() {
        eligibleBook_1 = ToBeCommandedBook.getBuilder()
                .addBook(BOOK_ID_1)
                .addNumberOfCopies(ELIGIBLE_NO_COPIES_1)
                .build();

        eligibleBook_2 = ToBeCommandedBook.getBuilder()
                .addBook(BOOK_ID_2)
                .addNumberOfCopies(ELIGIBLE_NO_COPIES_2)
                .build();

        final List<ToBeCommandedBook> allEligibleBooksList = new ArrayList<>() {
            {
                add(eligibleBook_1);
                add(eligibleBook_2);
            }
        };

        allEligibleCommand = Command.getBuilder()
                .addBooks(allEligibleBooksList)
                .addClient(CLIENT_ID)
                .build();

        client = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .build();

        final Class clientClass = client.getClass();
        try {
            final Field f = clientClass.getDeclaredField("id");
            f.setAccessible(true);
            f.set(client, CLIENT_ID);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        eligibleBookEntity_1 = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE_1)
                .addSubTitle(SUB_TITLE_1)
                .addIsbn(ISBN_1)
                .addPrice(PRICE_1)
                .build();

        final Class book1 = eligibleBookEntity_1.getClass();
        try {
            final Field f = book1.getDeclaredField("id");
            f.setAccessible(true);
            f.set(eligibleBookEntity_1, BOOK_ID_1);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        eligibleBookEntity_2 = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE_2)
                .addSubTitle(SUB_TITLE_2)
                .addIsbn(ISBN_2)
                .addPrice(PRICE_2)
                .build();

        final Class book2 = eligibleBookEntity_2.getClass();
        try {
            final Field f = book2.getDeclaredField("id");
            f.setAccessible(true);
            f.set(eligibleBookEntity_2, BOOK_ID_2);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        notEnoughBookEntity = BookEntity.getBuilder()
                .addMainTitle(MAIN_TITLE_3)
                .addIsbn(ISBN_3)
                .addPrice(PRICE_3)
                .build();

        final Class book3 = notEnoughBookEntity.getClass();
        try {
            final Field f = book3.getDeclaredField("id");
            f.setAccessible(true);
            f.set(notEnoughBookEntity, BOOK_ID_3);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        eligibleStockEntity1 = StockEntity.getBuilder()
                .addBookEntity(eligibleBookEntity_1)
                .addNumberOfCopies(100)
                .build();

        final Class stock1 = eligibleStockEntity1.getClass();
        try {
            final Field f = stock1.getDeclaredField("id");
            f.setAccessible(true);
            f.set(eligibleStockEntity1, STOCK_ID_1);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        eligibleStockEntity2 = StockEntity.getBuilder()
                .addBookEntity(eligibleBookEntity_2)
                .addNumberOfCopies(100)
                .build();

        final Class stock2 = eligibleStockEntity2.getClass();
        try {
            final Field f = stock2.getDeclaredField("id");
            f.setAccessible(true);
            f.set(eligibleStockEntity2, STOCK_ID_2);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }


        notEnoughStockEntity = StockEntity.getBuilder()
                .addBookEntity(notEnoughBookEntity)
                .addNumberOfCopies(1)
                .build();

        final Class stock3 = notEnoughStockEntity.getClass();
        try {
            final Field f = stock3.getDeclaredField("id");
            f.setAccessible(true);
            f.set(notEnoughStockEntity, STOCK_ID_3);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        commandEntityBeforeSave = CommandEntity.getBuilder()
                .addClient(client)
                .addTotalPrice(TOTAL_PRICE)
                .build();

        commandEntityAfterSave = CommandEntity.getBuilder()
                .addClient(client)
                .addTotalPrice(TOTAL_PRICE)
                .build();
        final Class commandClass = commandEntityAfterSave.getClass();
        try {
            final Field f = commandClass.getDeclaredField("id");
            f.setAccessible(true);
            f.set(commandEntityAfterSave, COMMAND_ID);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        final ToBeCommandedBook notFoundBook = ToBeCommandedBook.getBuilder()
                .addBook(1L)
                .addNumberOfCopies(1)
                .build();

        final ToBeCommandedBook notEnoughBook = ToBeCommandedBook.getBuilder()
                .addBook(BOOK_ID_3)
                .addNumberOfCopies(1450)
                .build();

        final List<ToBeCommandedBook> semiEligibleBookList = new ArrayList<>() {
            {
                add(eligibleBook_1);
                add(eligibleBook_2);
                add(notEnoughBook);
                add(notFoundBook);
            }
        };

        semiEligibleCommand = Command.getBuilder()
                .addBooks(semiEligibleBookList)
                .addClient(CLIENT_ID)
                .build();

        notEnoughReasonEntity = NonEligibilityReasonEntity.getBuilder()
                .addCode(NonEligibilityReasonEnum.NOT_ENOUGH_COPIES.name())
                .addLabel(NonEligibilityReasonEnum.NOT_ENOUGH_COPIES.getLabel())
                .addDescription(NonEligibilityReasonEnum.NOT_ENOUGH_COPIES.getDescription())
                .build();

        final Class notEnoughClass = notEnoughReasonEntity.getClass();
        try {
            final Field f = notEnoughClass.getDeclaredField("id");
            f.setAccessible(true);
            f.set(notEnoughReasonEntity, 4L);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        notFoundReasonEntity = NonEligibilityReasonEntity.getBuilder()
                .addCode(NonEligibilityReasonEnum.NOT_FOUND.name())
                .addLabel(NonEligibilityReasonEnum.NOT_FOUND.getLabel())
                .addDescription(NonEligibilityReasonEnum.NOT_FOUND.getDescription())
                .build();

        final Class notFoundClass = notFoundReasonEntity.getClass();
        try {
            final Field f = notFoundClass.getDeclaredField("id");
            f.setAccessible(true);
            f.set(notFoundReasonEntity, 5L);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        final List<ToBeCommandedBook> nonEligibleBookList = new ArrayList<>() {
            {
                add(notEnoughBook);
                add(notFoundBook);
            }
        };

        nonEligibleCommand = Command.getBuilder()
                .addBooks(nonEligibleBookList)
                .addClient(CLIENT_ID)
                .build();

        clientNotFoundCommand = Command.getBuilder()
                .addBooks(allEligibleBooksList)
                .addClient(-1L)
                .build();

        commandBookEntityList = new ArrayList<>();
        final CommandBookEntity commandBookEntity1 = CommandBookEntity.getBuilder()
                .addBookEntity(eligibleBookEntity_1)
                .addCommandEntity(commandEntityAfterSave)
                .addNumberOfCopies(12)
                .build();

        final CommandBookEntity commandBookEntity2 = CommandBookEntity.getBuilder()
                .addBookEntity(eligibleBookEntity_2)
                .addCommandEntity(commandEntityAfterSave)
                .addNumberOfCopies(12)
                .build();

        commandBookEntityList.add(commandBookEntity1);
        commandBookEntityList.add(commandBookEntity2);
    }


    @Test
    void when_command_contains_juts_eligible_items_then_command_should_be_created() {
        // given: all eligible command

        // when
        final CommandEntity command = commandService.createCommand(allEligibleCommand);

        // then
        assertNotNull(command);
        assertNotNull(command.getId());
        assertNotNull(command.getTotalPrice());
        assertEquals(TOTAL_PRICE, command.getTotalPrice());
    }

    @Test
    void when_command_contains_eligible_and_non_eligible_items_then_should_be_created() {
        // give: semiEligible command

        // when
        final CommandEntity command = commandService.createCommand(semiEligibleCommand);

        // then
        assertNotNull(command);
        assertNotNull(command.getId());
        assertNotNull(command.getTotalPrice());
        assertEquals(TOTAL_PRICE, command.getTotalPrice());
    }

    @Test
    void when_command_includes_just_non_valid_items_then_should_throw_ZetaIllegalStateException() {
        // given: the invalid command

        // when
        final ZetaIllegalStateException exception = assertThrows(
                ZetaIllegalStateException.class, () -> commandService.createCommand(nonEligibleCommand));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.ILLEGAL_COMMAND_MSG));
    }

    @Test
    void when_client_not_found_then_should_throw_ResourceNotFoundException() {
        // given: a no existing client id

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> commandService.createCommand(clientNotFoundCommand));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @Test
    void when_a_valid_commandEntity_given_then_should_list_books_included_in_command() {
        // given: a valid commandEntity

        // when
        final List<CommandedBook> commandedBooks = commandService.listBooksInCommand(commandEntityAfterSave);

        // then
        assertNotNull(commandedBooks);
        assertFalse(commandedBooks.isEmpty());
        assertEquals(2, commandedBooks.size());
    }

    @Test
    void when_listing_books_in_command_and_nonExisting_commandEntity_is_pased_then_should_throw_ResourceNotFoundException() {
        // given: a nonExisting commandEntity

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> commandService.listBooksInCommand(commandEntityBeforeSave));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @Test
    void when_finding_a_command_with_nonExisting_id_then_should_throw_ResourceNotFoundException() {
        // given: a nonExisting id

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> commandService.findCommandById(1L));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @TestConfiguration
    static class Configuration {
        @Primary
        @Bean
        CommandService commandService() {
            final CommandsRepository commandsRepository = mock(CommandsRepository.class);
            final CommandBookRepository commandBookRepository = mock(CommandBookRepository.class);
            final StockRepository stockRepository = mock(StockRepository.class);
            final BooksRepository booksRepository = mock(BooksRepository.class);
            final PersonRepository personRepository = mock(PersonRepository.class);
            final NonEligibleItemRepository nonEligibleItemRepository = mock(NonEligibleItemRepository.class);
            final NonEligibilityReasonRepository nonEligibilityReasonRepository = mock(NonEligibilityReasonRepository.class);

            when(personRepository.findById(CLIENT_ID)).thenReturn(Optional.of(client));

            when(booksRepository.findById(BOOK_ID_1)).thenReturn(Optional.of(eligibleBookEntity_1));
            when(booksRepository.findById(BOOK_ID_2)).thenReturn(Optional.of(eligibleBookEntity_2));
            when(booksRepository.findById(BOOK_ID_3)).thenReturn(Optional.of(notEnoughBookEntity));

            when(stockRepository.findByBookEntity(eligibleBookEntity_1)).thenReturn(Optional.of(eligibleStockEntity1));
            when(stockRepository.findByBookEntity(eligibleBookEntity_2)).thenReturn(Optional.of(eligibleStockEntity2));
            when(stockRepository.findByBookEntity(notEnoughBookEntity)).thenReturn(Optional.of(notEnoughStockEntity));

            when(commandsRepository.save(commandEntityBeforeSave)).thenReturn(commandEntityAfterSave);

            when(nonEligibilityReasonRepository.findByCode(NonEligibilityReasonEnum.NOT_FOUND.name())).thenReturn(Optional.of(notFoundReasonEntity));
            when(nonEligibilityReasonRepository.findByCode(NonEligibilityReasonEnum.NOT_ENOUGH_COPIES.name())).thenReturn(Optional.of(notEnoughReasonEntity));

            when(commandBookRepository.findByCommandEntity(commandEntityAfterSave)).thenReturn(Optional.of(commandBookEntityList));

            return new CommandServiceImpl(commandsRepository,
                    commandBookRepository,
                    stockRepository,
                    booksRepository,
                    personRepository,
                    nonEligibleItemRepository, nonEligibilityReasonRepository);
        }
    }
}
