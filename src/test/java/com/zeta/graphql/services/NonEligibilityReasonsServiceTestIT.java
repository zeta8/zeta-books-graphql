package com.zeta.graphql.services;

import com.zeta.graphql.GraphqlApplication;
import com.zeta.graphql.entities.NonEligibilityReasonEntity;
import com.zeta.graphql.enums.NonEligibilityReasonEnum;
import com.zeta.graphql.repositories.NonEligibilityReasonRepository;
import com.zeta.graphql.services.interfaces.NonEligibilityReasonsService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {GraphqlApplication.class, NonEligibilityReasonsServiceTestIT.Configuration.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class NonEligibilityReasonsServiceTestIT {

    @Autowired
    private NonEligibilityReasonsService nonEligibilityReasonsService;

    private static NonEligibilityReasonEntity nonEligibilityReasonEntityBeforeSave;
    private static NonEligibilityReasonEntity nonEligibilityReasonEntityAfterSave;
    private static NonEligibilityReasonEntity nonEligibilityReasonEntityExiting;
    private static final NonEligibilityReasonEnum REASON = NonEligibilityReasonEnum.NOT_ENOUGH_COPIES;
    private static final NonEligibilityReasonEnum REASON_NONEXISTENT = NonEligibilityReasonEnum.NOT_FOUND;

    @BeforeAll
    static void init() {
        nonEligibilityReasonEntityBeforeSave = NonEligibilityReasonEntity.getBuilder()
                .addCode(REASON_NONEXISTENT.name())
                .addLabel(REASON_NONEXISTENT.getLabel())
                .addDescription(REASON_NONEXISTENT.getDescription())
                .build();

        nonEligibilityReasonEntityAfterSave = NonEligibilityReasonEntity.getBuilder()
                .addCode(REASON_NONEXISTENT.name())
                .addLabel(REASON_NONEXISTENT.getLabel())
                .addDescription(REASON_NONEXISTENT.getDescription())
                .build();

        final Class clazz1 = nonEligibilityReasonEntityAfterSave.getClass();
        try {
            final Field f = clazz1.getDeclaredField("id");
            f.setAccessible(true);
            f.set(nonEligibilityReasonEntityAfterSave, 12L);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        nonEligibilityReasonEntityExiting = NonEligibilityReasonEntity.getBuilder()
                .addCode(REASON.name())
                .addLabel(REASON.getLabel())
                .addDescription(REASON.getDescription())
                .build();

        final Class clazz = nonEligibilityReasonEntityExiting.getClass();
        try {
            final Field f = clazz.getDeclaredField("id");
            f.setAccessible(true);
            f.set(nonEligibilityReasonEntityExiting, 1L);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    @Test
    void when_the_true_file_path_given_then_adds_to_database() {
        // given: the fileName

        // when
        final List<NonEligibilityReasonEntity> nonEligibilityReasonEntities = nonEligibilityReasonsService.addNonEligibilityReasonsFromFile();

        // then
        assertNotNull(nonEligibilityReasonEntities);
        assertFalse(nonEligibilityReasonEntities.isEmpty());
        assertEquals(2, nonEligibilityReasonEntities.size());
    }

    @TestConfiguration
    static class Configuration {
        @Primary
        @Bean
        NonEligibilityReasonsService nonEligibilityReasonsService() {
            final NonEligibilityReasonRepository nonEligibilityReasonRepository = mock(NonEligibilityReasonRepository.class);

            when(nonEligibilityReasonRepository.findByCode(REASON.name())).thenReturn(Optional.of(nonEligibilityReasonEntityExiting));
            when(nonEligibilityReasonRepository.save(nonEligibilityReasonEntityBeforeSave)).thenReturn(nonEligibilityReasonEntityAfterSave);

            return new NonEligibilityReasonsServiceImpl(nonEligibilityReasonRepository);
        }
    }
}
