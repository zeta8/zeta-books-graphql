package com.zeta.graphql.services;

import com.zeta.graphql.GraphqlApplication;
import com.zeta.graphql.entities.CommandEntity;
import com.zeta.graphql.entities.NonEligibilityReasonEntity;
import com.zeta.graphql.entities.NonEligibleItemEntity;
import com.zeta.graphql.entities.PersonEntity;
import com.zeta.graphql.enums.NonEligibilityReasonEnum;
import com.zeta.graphql.repositories.NonEligibleItemRepository;
import com.zeta.graphql.services.interfaces.NonEligibleItemsService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {GraphqlApplication.class, NonEligibleItemsServiceTestsIT.Configuration.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class NonEligibleItemsServiceTestsIT {

    @Autowired
    private NonEligibleItemsService nonEligibleItemsService;

    private static final String PERSON_CODE = "CD-1";
    private static final String FIRST_NAME = "Wolfgang";
    private static final String MIDDLE_NAME = "Amadeus";
    private static final String LAST_NAME = "MOZART";
    private static final LocalDate BIRTH_DATE = LocalDate.of(1756, 1, 27);
    private static final BigDecimal TOTAL_PRICE = new BigDecimal("42.65");

    private static CommandEntity command;
    private static PersonEntity client;
    private static final Long CLIENT_ID = 1L;

    private static CommandEntity commandWithoutNonEligibleItems;

    private static NonEligibleItemEntity nonEligibleItem;
    private static List<NonEligibleItemEntity> nonEligibleItems;

    @BeforeAll
    static void init() {
        client = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .build();

        final Class clazz = client.getClass();
        try {
            final Field f = clazz.getDeclaredField("id");
            f.setAccessible(true);
            f.set(client, CLIENT_ID);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        command = CommandEntity
                .getBuilder()
                .addTotalPrice(TOTAL_PRICE)
                .addClient(client)
                .build();

        final Class commandClass = command.getClass();
        try {
            final Field f = commandClass.getDeclaredField("id");
            f.setAccessible(true);
            f.set(command, 25L);
        } catch (NoSuchFieldException |IllegalAccessException e) {
            e.printStackTrace();
        }

        commandWithoutNonEligibleItems = CommandEntity.getBuilder()
                .addTotalPrice(new BigDecimal("14.37"))
                .addClient(client)
                .build();

        final NonEligibilityReasonEntity reason = NonEligibilityReasonEntity.getBuilder()
                .addCode(NonEligibilityReasonEnum.NOT_FOUND.name())
                .addLabel(NonEligibilityReasonEnum.NOT_FOUND.getLabel())
                .addDescription(NonEligibilityReasonEnum.NOT_FOUND.getDescription())
                .build();

        nonEligibleItem = NonEligibleItemEntity.getBuilder()
                .addNonExistingBook(1L)
                .addCommand(command)
                .addReason(reason)
                .build();

        nonEligibleItems = new ArrayList<>();
        nonEligibleItems.add(nonEligibleItem);
    }

    @Test
    void when_there_is_nonEligible_item_in_command_then_should_return_it() {
        // given: all inputs

        // when
        final List<NonEligibleItemEntity> nonEligibleItemsByCommand = nonEligibleItemsService.findNonEligibleItemsByCommand(command);

        // then
        assertNotNull(nonEligibleItemsByCommand);
        assertFalse(nonEligibleItemsByCommand.isEmpty());
    }

    @Test
    void when_there_is_non_nonEligible_item_in_command_then_should_return_an_empty_list() {
        // given: all inputs

        // when
        final List<NonEligibleItemEntity> nonEligibleItemsByCommand = nonEligibleItemsService.findNonEligibleItemsByCommand(commandWithoutNonEligibleItems);

        // then
        assertNotNull(nonEligibleItemsByCommand);
        assertTrue(nonEligibleItemsByCommand.isEmpty());
    }

    @TestConfiguration
    static class Configuration {
        @Primary
        @Bean
        NonEligibleItemsService nonEligibleItemsService() {
            final NonEligibleItemRepository nonEligibleItemRepository = mock(NonEligibleItemRepository.class);

            when(nonEligibleItemRepository.findByCommand(command)).thenReturn(Optional.of(nonEligibleItems));

            return new NonEligibleItemsServiceImpl(nonEligibleItemRepository);
        }
    }
}
