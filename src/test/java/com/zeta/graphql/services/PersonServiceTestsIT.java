package com.zeta.graphql.services;

import com.zeta.graphql.GraphqlApplication;
import com.zeta.graphql.dtos.PersonInformation;
import com.zeta.graphql.entities.PersonEntity;
import com.zeta.graphql.entities.PersonStatusEntity;
import com.zeta.graphql.entities.StatusEntity;
import com.zeta.graphql.entities.embeddables.PersonStatusId;
import com.zeta.graphql.enums.StatusEnum;
import com.zeta.graphql.exceptions.ResourceNotFoundException;
import com.zeta.graphql.repositories.PersonRepository;
import com.zeta.graphql.repositories.PersonStatusRepository;
import com.zeta.graphql.repositories.StatusRepository;
import com.zeta.graphql.services.interfaces.PersonService;
import com.zeta.graphql.toolbox.TestErrorConstants;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {GraphqlApplication.class, PersonServiceTestsIT.Configuration.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PersonServiceTestsIT {

    @Autowired
    private PersonService personService;

    private static final String PERSON_CODE = "CD-1";
    private static final String FIRST_NAME = "Wolfgang";
    private static final String MIDDLE_NAME = "Amadeus";
    private static final String LAST_NAME = "MOZART";
    private static final LocalDate BIRTH_DATE = LocalDate.of(1756, 1, 27);
    private static final StatusEnum STATUS = StatusEnum.WRI;
    private static final Long STATUS_ID = 1L;
    private static final Long PERSON_ID = 2L;

    private static PersonEntity beforeSavePerson;
    private static PersonEntity afterSavePerson;
    private static StatusEntity savedStatus;
    private static PersonStatusEntity beforeSavePersonStatus;
    private static PersonStatusEntity afterSavePersonStatus;



    @BeforeAll
    static void init() {

        beforeSavePerson = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .build();

        afterSavePerson = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .build();

        savedStatus = StatusEntity.getBuilder()
                .addCode(STATUS.name())
                .addLabel(STATUS.getLabel())
                .addDescription(STATUS.getDescription())
                .build();

        beforeSavePersonStatus = PersonStatusEntity.getBuilder()
                .addStatusEntity(savedStatus)
                .addPersonEntity(afterSavePerson)
                .build();

        afterSavePersonStatus = PersonStatusEntity.getBuilder()
                .addStatusEntity(savedStatus)
                .addPersonEntity(afterSavePerson)
                .build();

        final Class statusClass = savedStatus.getClass();
        try {
            final Field f = statusClass.getDeclaredField("id");
            f.setAccessible(true);
            f.set(savedStatus, STATUS_ID);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        final Class personClass = afterSavePerson.getClass();
        try {
            final Field f = personClass.getDeclaredField("id");
            f.setAccessible(true);
            f.set(afterSavePerson, PERSON_ID);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        PersonStatusId personStatusId = PersonStatusId.getNewInstance();
        personStatusId.setStatusId(savedStatus.getId());
        personStatusId.setPersonId(afterSavePerson.getId());

        final Class personStatusClass = afterSavePersonStatus.getClass();
        try {
            final Field f = personStatusClass.getDeclaredField("personStatusId");
            f.setAccessible(true);
            f.set(afterSavePersonStatus, personStatusId);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    void when_creating_person_with_existing_status_then_should_be_ok() {
        // given:
        final PersonInformation personInformation = PersonInformation.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(STATUS)
                .build();

        // when
        final PersonEntity person = personService.createPerson(personInformation);

        // then
        assertNotNull(person);
        assertEquals(FIRST_NAME, person.getFirstName());
        assertEquals(MIDDLE_NAME, person.getMiddleName());
        assertEquals(LAST_NAME, person.getLastName());
        assertEquals(BIRTH_DATE, person.getBirthDate().toLocalDate());
        assertNotNull(person.getId());
    }

    @Test
    void when_creating_person_and_status_not_exists_the_should_throw_ResourceNotFoundException() {
        // given:
        final PersonInformation person = PersonInformation.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .addStatus(StatusEnum.CUS)
                .build();

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> personService.createPerson(person));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @Test
    void when_finding_a_person_with_existing_id_then_should_be_ok() {
        // given: The existing id of person

        // when
        final PersonEntity person = personService.findPersonById(PERSON_ID);

        // then
        assertNotNull(person);
        assertEquals(FIRST_NAME, person.getFirstName());
        assertEquals(MIDDLE_NAME, person.getMiddleName());
        assertEquals(LAST_NAME, person.getLastName());
        assertEquals(BIRTH_DATE, person.getBirthDate().toLocalDate());
    }

    @Test
    void when_finding_a_person_with_nonExisting_id_then_should_throw_ResourceNotFoundException() {
        // given: The non existing id
        final Long nonExistingId = -1L;

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> personService.findPersonById(nonExistingId));

        // when
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @Test
    void when_finding_a_person_with_existing_code_then_should_be_ok() {
        // given: The existing id of person

        // when
        final PersonEntity person = personService.findPersonByCode(PERSON_CODE);

        // then
        assertNotNull(person);
        assertEquals(FIRST_NAME, person.getFirstName());
        assertEquals(MIDDLE_NAME, person.getMiddleName());
        assertEquals(LAST_NAME, person.getLastName());
        assertEquals(BIRTH_DATE, person.getBirthDate().toLocalDate());
    }

    @Test
    void when_finding_a_person_with_nonExisting_code_then_should_throw_ResourceNotFoundException() {
        // given: The non existing id
        final String nonExistingCode = "hfjahhezihzez";

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> personService.findPersonByCode(nonExistingCode));

        // when
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @TestConfiguration
    static class Configuration {
        @Primary
        @Bean
        PersonService personService() {
            final StatusRepository statusRepository = mock(StatusRepository.class);
            final PersonRepository personRepository = mock(PersonRepository.class);
            final PersonStatusRepository personStatusRepository = mock(PersonStatusRepository.class);

            when(statusRepository.findByCode(STATUS.name())).thenReturn(Optional.of(savedStatus));
            when(personRepository.save(beforeSavePerson)).thenReturn(afterSavePerson);
            when(personStatusRepository.save(beforeSavePersonStatus)).thenReturn(afterSavePersonStatus);

            when(personRepository.findById(PERSON_ID)).thenReturn(Optional.of(afterSavePerson));
            when(personRepository.findByPersonCode(PERSON_CODE)).thenReturn(Optional.of(afterSavePerson));

            return new PersonServiceImpl(statusRepository, personRepository, personStatusRepository);


        }
    }
}
