package com.zeta.graphql.services;

import com.zeta.graphql.GraphqlApplication;
import com.zeta.graphql.entities.PersonEntity;
import com.zeta.graphql.entities.PersonStatusEntity;
import com.zeta.graphql.entities.StatusEntity;
import com.zeta.graphql.entities.embeddables.PersonStatusId;
import com.zeta.graphql.enums.StatusEnum;
import com.zeta.graphql.exceptions.ResourceNotFoundException;
import com.zeta.graphql.repositories.PersonRepository;
import com.zeta.graphql.repositories.PersonStatusRepository;
import com.zeta.graphql.services.interfaces.PersonStatusService;
import com.zeta.graphql.toolbox.TestErrorConstants;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {GraphqlApplication.class, PersonStatusServiceTestsIT.Configuration.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PersonStatusServiceTestsIT {

    @Autowired
    private PersonStatusService personStatusService;

    private static final String PERSON_CODE = "CD-1";
    private static final String FIRST_NAME = "Wolfgang";
    private static final String MIDDLE_NAME = "Amadeus";
    private static final String LAST_NAME = "MOZART";
    private static final LocalDate BIRTH_DATE = LocalDate.of(1756, 1, 27);


    private static PersonEntity personEntitySaved;
    private static PersonEntity personWithoutStatusEntity;
    private static final Long PERSON_ENTITY_SAVED_ID = 1L;
    private static final Long PERSON_WITH_WITHOUT_STATUS_ID = 10L;
    private static PersonStatusEntity personStatusEntitySaved;
    private static PersonStatusEntity personStatusEntityBeforeSave;
    private static StatusEntity statusEntitySaved;
    private static final StatusEnum STATUS = StatusEnum.WRI;
    private static final Long STATUS_ID = 2L;
    private static final Long PERSON_STATUS_ID = 3L;

    @BeforeAll
    static void init() {
        personEntitySaved = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .build();

        final Class clazz = personEntitySaved.getClass();
        try {
            final Field f = clazz.getDeclaredField("id");
            f.setAccessible(true);
            f.set(personEntitySaved, PERSON_ENTITY_SAVED_ID);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        statusEntitySaved = StatusEntity.getBuilder()
                .addCode(STATUS.name())
                .addLabel(STATUS.getLabel())
                .addDescription(STATUS.getDescription())
                .build();

        final Class clazz_1 = statusEntitySaved.getClass();
        try {
            final Field f = clazz_1.getDeclaredField("id");
            f.setAccessible(true);
            f.set(statusEntitySaved, STATUS_ID);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        personStatusEntitySaved = PersonStatusEntity.getBuilder()
                .addPersonEntity(personEntitySaved)
                .addStatusEntity(statusEntitySaved)
                .build();

        personStatusEntityBeforeSave = PersonStatusEntity.getBuilder()
                .addPersonEntity(personEntitySaved)
                .addStatusEntity(statusEntitySaved)
                .build();

        PersonStatusId personStatusId = PersonStatusId.getNewInstance();
        personStatusId.setPersonId(PERSON_ENTITY_SAVED_ID);
        personStatusId.setStatusId(STATUS_ID);

        final Class clazz_2 = personStatusEntitySaved.getClass();
        try {
            final Field f = clazz_2.getDeclaredField("personStatusId");
            f.setAccessible(true);
            f.set(personStatusEntitySaved, personStatusId);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }


        personWithoutStatusEntity = PersonEntity.getBuilder()
                .addPersonCode(PERSON_CODE)
                .addFirstName(FIRST_NAME)
                .addMiddleName(MIDDLE_NAME)
                .addLastName(LAST_NAME)
                .addBirthDate(BIRTH_DATE)
                .build();

        final Class clazz_3 =  personWithoutStatusEntity.getClass();
        try {
            final Field f = clazz_3.getDeclaredField("id");
            f.setAccessible(true);
            f.set(personWithoutStatusEntity, PERSON_WITH_WITHOUT_STATUS_ID);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    @Test
    void when_finding_personStatusEntity_for_existing_personId_and_personStatus_exists_then_should_be_ok() {
        // given all inputsPERSON_ENTITY_SAVED_ID

        // when
        final PersonStatusEntity byPersonId = personStatusService.findByPersonId(PERSON_ENTITY_SAVED_ID);

        // then
        assertNotNull(byPersonId);
        assertNotNull(personEntitySaved.getId());
        assertEquals(personEntitySaved, byPersonId.getPersonEntity());
        assertEquals(statusEntitySaved, byPersonId.getStatusEntity());
    }

    @Test
    void when_finding_personStatusEntity_for_non_exiting_personId_then_should_throw_ResourceNotFoundException() {
        // given: all inputs

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> personStatusService.findByPersonId(1000L));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }


    @Test
    void when_finding_personStatusEntity_for_existing_personId_and_personStatus_does_not_exists_then_should_throw_ResourceNotFoundException() {
        // given all inputs

        // when

        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> personStatusService.findByPersonId(PERSON_WITH_WITHOUT_STATUS_ID));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }


    @Test
    void when_finding_by_personEntity_and_personEntity_nonExisting_then_should_throw_ResourceNotFoundException() {
        // given: all the inputs

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> personStatusService.findByPersonEntity(personWithoutStatusEntity));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @Test
    void when_saving_a_personStatus_with_valid_input_the_should_be_ok() {
        // given: the inputs

        // when
        final PersonStatusEntity personStatus = personStatusService.createPersonStatus(personEntitySaved, statusEntitySaved);

        // then
        assertNotNull(personStatus);
        assertEquals(personEntitySaved, personStatus.getPersonEntity());
        assertEquals(statusEntitySaved, personStatus.getStatusEntity());
        assertNotNull(personStatus.getPersonStatusId());
    }

    @Test
    void when_finding_statusEntity_and_person_id_nonExisting_then_should_throw_ResourceNotFoundException() {
        // given nonExisting person id
        final Long nonExistingPersonId = -1L;

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> personStatusService.findForPersonId(nonExistingPersonId));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @Test
    void when_finding_statusEntity_and_person_id_exits_but_PersonStatusEntity_nonExisting_then_should_throw_ResourceNotFoundException() {
        // given the input

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> personStatusService.findForPersonId(PERSON_WITH_WITHOUT_STATUS_ID));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @Test
    void when_finding_statusEntity_with_existing_person_and_PersonStatusEntity_then_should_be_ok() {
        // given the input

        // when
        final StatusEntity statusEntity = personStatusService.findForPersonId(PERSON_ENTITY_SAVED_ID);

        // then
        assertNotNull(statusEntity);
        assertEquals(STATUS.name(), statusEntity.getCode());
        assertEquals(STATUS.getLabel(), statusEntity.getLabel());
        assertEquals(STATUS.getDescription(), statusEntity.getDescription());
    }

    @TestConfiguration
    static class Configuration {
        @Primary
        @Bean
        PersonStatusService personStatusService() {
            final PersonRepository personRepository = mock(PersonRepository.class);
            final PersonStatusRepository personStatusRepository = mock(PersonStatusRepository.class);

            when(personRepository.findById(PERSON_ENTITY_SAVED_ID)).thenReturn(Optional.of(personEntitySaved));
            when(personRepository.findById(PERSON_WITH_WITHOUT_STATUS_ID)).thenReturn(Optional.of(personWithoutStatusEntity));
            when(personStatusRepository.findByPersonEntity(personEntitySaved)).thenReturn(Optional.of(personStatusEntitySaved));
            when(personStatusRepository.save(personStatusEntityBeforeSave)).thenReturn(personStatusEntitySaved);
            return new PersonStatusServiceImpl(personRepository, personStatusRepository);
        }
    }
}
