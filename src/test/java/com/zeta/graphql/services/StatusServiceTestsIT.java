package com.zeta.graphql.services;

import com.zeta.graphql.GraphqlApplication;
import com.zeta.graphql.dtos.StatusDto;
import com.zeta.graphql.entities.StatusEntity;
import com.zeta.graphql.exceptions.ResourceNotFoundException;
import com.zeta.graphql.repositories.StatusRepository;
import com.zeta.graphql.services.interfaces.StatusService;
import com.zeta.graphql.toolbox.TestErrorConstants;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.lang.reflect.Field;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = {GraphqlApplication.class, StatusServiceTestsIT.Configuration.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class StatusServiceTestsIT {

    @Autowired
    private StatusService statusService;

    private static final String EXISTING_STATUS_CODE = "EX-ST-CODE";
    private static final String NON_EXISTING_STATUS_CODE = "NON-EXT-CODE";
    private static final Long EXISTING_STATUS_ID = 1L;
    private static final Long NON_EXISTING_STATUS_ID = 2L;

    private static final String STATUS_LABEL = "LABEL";
    private static final String STATUS_DESCRIPTION = "DESCRIPTION";

    private static StatusEntity statusEntityBeforeSave;
    private static StatusEntity statusEntityAfterSave;
    private static StatusDto statusDto;

    @BeforeAll
    static void init() {
        statusEntityBeforeSave = StatusEntity.getBuilder()
                .addCode(EXISTING_STATUS_CODE)
                .addLabel(STATUS_LABEL)
                .addDescription(STATUS_DESCRIPTION)
                .build();

        statusEntityAfterSave = StatusEntity.getBuilder()
                .addCode(EXISTING_STATUS_CODE)
                .addLabel(STATUS_LABEL)
                .addDescription(STATUS_DESCRIPTION)
                .build();

        final Class clazz = statusEntityAfterSave.getClass();
        try {
            final Field f = clazz.getDeclaredField("id");
            f.setAccessible(true);
            f.set(statusEntityAfterSave, EXISTING_STATUS_ID);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    void when_proper_input_then_should_create_the_statusEntity() {
        // given
        statusDto = StatusDto.getBuilder()
                .addCode(EXISTING_STATUS_CODE)
                .addLabel(STATUS_LABEL)
                .addDescription(STATUS_DESCRIPTION)
                .build();

        // when
        final StatusEntity status = statusService.createStatus(statusDto);

        // then
        assertNotNull(status);
        assertEquals(EXISTING_STATUS_ID, status.getId());
        assertEquals(EXISTING_STATUS_CODE, status.getCode());
        assertEquals(STATUS_LABEL, status.getLabel());
        assertEquals(STATUS_LABEL, status.getLabel());
    }

    @Test
    void when_finding_statusEntity_with_existing_id_then_should_be_ok() {
        // given: the id for existing status

        // when
        final StatusEntity statusById = statusService.findStatusById(EXISTING_STATUS_ID);

        // then
        assertNotNull(statusById);
        assertEquals(EXISTING_STATUS_ID, statusById.getId());
        assertEquals(EXISTING_STATUS_CODE, statusById.getCode());
        assertEquals(STATUS_LABEL, statusById.getLabel());
        assertEquals(STATUS_DESCRIPTION, statusById.getDescription());
    }

    @Test
    void when_finding_statusEntity_with_existing_code_then_should_be_ok() {
        // given: the id for existing status

        // when
        final StatusEntity statusById = statusService.findStatusByCode(EXISTING_STATUS_CODE);

        // then
        assertNotNull(statusById);
        assertEquals(EXISTING_STATUS_ID, statusById.getId());
        assertEquals(EXISTING_STATUS_CODE, statusById.getCode());
        assertEquals(STATUS_LABEL, statusById.getLabel());
        assertEquals(STATUS_DESCRIPTION, statusById.getDescription());
    }

    @Test
    void when_finding_statusEntity_by_nonExisting_id_then_should_throw_ResourceNotFoundException() {
        // given: non existing status id

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> statusService.findStatusById(NON_EXISTING_STATUS_ID));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }

    @Test
    void when_finding_statusEntity_by_nonExisting_code_then_should_throw_ResourceNotFoundException() {
        // given: non existing status id

        // when
        final ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> statusService.findStatusByCode(NON_EXISTING_STATUS_CODE));

        // then
        final String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TestErrorConstants.NOT_FOUND_MSG));
    }



    @TestConfiguration
    static class Configuration {
        @Primary
        @Bean
        StatusService statusService() {
            final StatusRepository statusRepository = mock(StatusRepository.class);

            when(statusRepository.save(statusEntityBeforeSave)).thenReturn(statusEntityAfterSave);
            when(statusRepository.findById(EXISTING_STATUS_ID)).thenReturn(Optional.of(statusEntityAfterSave));
            when(statusRepository.findByCode(EXISTING_STATUS_CODE)).thenReturn(Optional.of(statusEntityAfterSave));

            return new StatusServiceImpl(statusRepository);
        }
    }
}
