package com.zeta.graphql.toolbox;

public class TestErrorConstants {
    public static final String MANDATORY_VALUE_MSG = "is mandatory";
    public static final String ALREADY_FED_MSG = "has been already fed in";
    public static final String DUAL_BUILD_MSG = "is already built, it cannot be rebuilt";
    public static final String FED_AFTER_INSTANTIATION_MSG = "Feeding in the fields is not permitted after the object is built";
    public static final String NOT_FOUND_MSG = "not found";
    public static final String ILLEGAL_COMMAND_MSG = "There are no valid items in the command";
    public static final String INCOHERENT_VALUE_MSG = "Exactly one of the fields";
    public static final String COUPLED_FIELDS_MSG = "should accompany the value of";
    public static final String NON_ACCUMULATIVE_FIELD_ERR = "Exactly one of the fields";
    public static final String ONE_FILED_MANDATORY = "At least one of the fields";
    public static String COMMAND_NOT_VALID = "There are no valid items in the command";
}
