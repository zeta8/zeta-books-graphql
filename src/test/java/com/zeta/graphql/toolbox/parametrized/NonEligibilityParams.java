package com.zeta.graphql.toolbox.parametrized;

import com.zeta.graphql.entities.NonEligibilityReasonEntity;
import com.zeta.graphql.enums.NonEligibilityReasonEnum;

import java.util.stream.Stream;

public class NonEligibilityParams {
    static Stream<NonEligibilityReasonEntity> bothReasons() {
        final NonEligibilityReasonEnum notEnoughCopiesReason = NonEligibilityReasonEnum.NOT_ENOUGH_COPIES;
        final NonEligibilityReasonEnum notFoundReason = NonEligibilityReasonEnum.NOT_FOUND;

        final NonEligibilityReasonEntity nonEligibilityNotEnough = NonEligibilityReasonEntity.getBuilder()
                .addCode(notEnoughCopiesReason.name())
                .addLabel(notEnoughCopiesReason.getLabel())
                .addDescription(notEnoughCopiesReason.getDescription())
                .build();

        final NonEligibilityReasonEntity nonEligibilityNotFound = NonEligibilityReasonEntity.getBuilder()
                .addCode(notFoundReason.name())
                .addLabel(notFoundReason.getLabel())
                .addDescription(notEnoughCopiesReason.getDescription())
                .build();
        return Stream.of(nonEligibilityNotEnough, nonEligibilityNotFound);
    }
}
